import { AxiosError } from 'axios';

export const httpErrorLogger = (apiName: string, apiErrorCode: string, error: AxiosError) => {
  if (process.env.NODE_ENV && process.env.NODE_ENV === 'development') {
    console.log(`httpErrorLogger: API-ERROR-${apiName}-${apiErrorCode}`, error);
  }

  return null;
  // TODO SENTRY
};
