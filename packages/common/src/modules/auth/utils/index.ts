import * as Yup from 'yup';

import { forms } from '../constants';

export type LoginFormValues = Yup.InferType<typeof forms.login.schema> & {
  submit?: string;
};
export type ForgotPasswordFormValues = Yup.InferType<
  typeof forms.forgotPassword.schema
>;
export type ResetPasswordFormValues = Yup.InferType<
  typeof forms.resetPassword.schema
>;
export type VerificationCodeFormValues = Yup.InferType<
  typeof forms.verificationCode.schema
> & {
  submit?: string;
};

// Login API
export type LoginPayload = {
  email?: string;
  username: string;
  password: string;
};

export type LoginResponse = {
  username: string;
  uuid: string;
  authorizationToken: string;
  tokenType: string;
  refreshToken: string;
  expires: number;
  scope: {
    isFunded: boolean;
    applicationId: string;
    contactId: string;
    email: string;
    firstName?: string;
    lastName?: string;
  };
  applications: Array<{ applicationId: number }>;
};

export type ResumptiveUserStatus = {
  isFunded?: boolean;
  email: string;
  firstName?: string;
  lastName?: string;
};
