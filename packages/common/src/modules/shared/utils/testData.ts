export const userInfo = {
  id: 'd74e2532-2651-4ebf-a6a4-0d25e579717d',
  firstName: 'John',
  lastName: 'Smith',
  email: 'test@mail.com',
  phoneNumber: '(321) 654-9870',
  street: '220 LOCUS AVE',
  city: 'Atlanta',
  state: 'GA',
  zipCode: '30318',
  employmentType: 'Employee',
  employerName: '',
  annualIncome: 25000.0,
};
