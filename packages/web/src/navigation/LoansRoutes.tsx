import React, { lazy } from 'react';
import { Route, useRouteMatch, Switch } from 'react-router-dom';

// Lazy Routes
const Loans = lazy(() => import('modules/loans/screens/Loans'));
const LoanDetails = lazy(() => import('modules/loans/screens/LoanDetails'));
const PaymentsHistory = lazy(() => import('modules/loans/screens/PaymentsHistory'));
const AutopaySetup = lazy(() => import('modules/payments/screens/AutopaySetup'));

const LoansRoutes = () => {

  let { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={path} component={Loans} exact />
      <Route path={`${path}/loanDetails`} component={LoanDetails} exact />
      <Route path={`${path}/loanDetails/makePayment`} component={LoanDetails} />
      <Route path={`${path}/loanDetails/paymentsHistory`} component={PaymentsHistory} />
      <Route path={`${path}/loanDetails/autopaySetup`} component={AutopaySetup} />
    </Switch>
  );
};

export default LoansRoutes;