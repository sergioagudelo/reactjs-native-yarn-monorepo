import React, { lazy } from 'react';
import { Route, Switch } from 'react-router-dom';

// Routers
import AuthRoutes from './AuthRoutes';
import LoanRoutes from './LoansRoutes';

// Components
// import Profile from '../modules/profile/Profile';
import { PaymentReceipt } from '../modules/payments/screens/';

// Lazy Routes
const Profile = lazy(() => import('modules/profile/screens/Profile'));

// REV: how to nest routes by module
// const routes = [
//   {
//     path : "/",
//     component: Profile,
//      exact: true,
//   },
//   {
//     path : '/auth',
//     routes: [
//       {
//         path : '/auth/login',
//         component: Login
//       },
//       {
//         path: '/auth/forgot-password',
//         component: ForgotPassword
//       }
//     ]
//   }
// ]

const RootRoutes = () => {
  return (
    <Switch>
      <Route path="/auth" component={AuthRoutes} />
      <Route path="/loans" component={LoanRoutes} />
      <Route path="/" exact component={Profile} />
      <Route path="/profile" exact component={Profile} />
      <Route path="/paymentReceipt" component={PaymentReceipt} />
      {/* {routes.map((route, i) => (
          <Route path={route.path} component={route.component} key={i} />
        ))} */}
    </Switch>
  );
};

export default RootRoutes;
