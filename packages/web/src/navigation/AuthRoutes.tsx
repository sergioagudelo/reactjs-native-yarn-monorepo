import React, { lazy } from 'react';
import { Route, useRouteMatch, Switch } from 'react-router-dom';
import '../modules/auth/styles/account-forms.scss'
import '../modules/auth/styles/billboard.scss'

// Components
import Login from 'modules/auth/screens/Login'
import ForgotPasswordSuccess from '../modules/auth/screens/ForgotPasswordSuccess';

// Lazy Routes
const ForgotPassword = lazy(() => import('modules/auth/screens/ForgotPassword'));

const AuthRoutes = () => {

  let { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/forgot-password`} component={ForgotPassword} />
      <Route path={path} exact component={Login} />
      <Route path={`${path}/email-send`} exact component={ForgotPasswordSuccess} />
    </Switch>
  );
};

export default AuthRoutes;