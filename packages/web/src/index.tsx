import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

// UIKIT
import 'uikit/dist/css/uikit.min.css'
import 'uikit/dist/js/uikit.min.js'
import 'uikit/dist/js/uikit-icons.min.js'

import 'assets/styles/custom.scss';

// REV: Reactotron. Needs to be implemented.
// if (__DEV__) {
//   // import('./src/config/ReactotronConfig').then(() => console.log('Reactotron Configured'));
//   import('./src/config/ReactotronConfig').then(() => null);
// }

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
