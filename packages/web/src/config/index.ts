const config = {
  environment: process.env.REACT_APP_ENVIRONMENT_NAME as any,
  disablePush: !!process.env.REACT_APP_DISABLE_PUSH as any,
  disableForceUpdate: !!process.env.REACT_APP_DISABLE_FORCE_UPDATE as any,
  disableAndroidBiometrics: !!process.env.REACT_APP_DISABLE_ANDROID_BIOMETRICS as any,
  disableAnalytics: !!process.env.REACT_APP_DISABLE_ANALYTICS as any,
  sentry: {
    dsn: process.env.REACT_APP_SENTRY_DSN as any,
  },
  api: {
    host: process.env.NODE_ENV && process.env.NODE_ENV === 'development' ?
      process.env.REACT_APP_API_HOST as any : process.env.REACT_APP_API_HOST as any,
    paths: {
      consumer: '/consumer/v1',
      manager: '/api-manager',
    },
    apiManager: {
      managerAuth: {
        clientId: process.env.REACT_APP_CLIENT_ID as any,
        clientSecret: process.env.REACT_APP_CLIENT_SECRET as any,
        password: process.env.REACT_APP_MANAGER_PASSWORD as any,
        username: process.env.REACT_APP_MANAGER_USERNAME as any,
      },
      appToken: process.env.REACT_APP_MANAGER_APP_TOKEN as any,
    },
    sdocs: {
      authKey: process.env.REACT_APP_SDOCS_AUTH_KEY as any,
      appToken: process.env.REACT_APP_SDOCS_APP_TOKEN as any,
    },
    appVersions: {
      grantType: process.env.REACT_APP_APP_VERSIONS_GRANT_TYPE as any,
      username: process.env.REACT_APP_APP_VERSIONS_USERNAME as any,
      password: process.env.REACT_APP_APP_VERSIONS_PASSWORD as any,
      clientId: process.env.REACT_APP_APP_VERSIONS_CLIENT_ID as any,
      clientSecret: process.env.REACT_APP_APP_VERSIONS_CLIENT_SECRET as any,
    }
    // apiKey: process.env.REACT_APP_API_KEY as any,
  },
  lpApps: {
    host: process.env.REACT_APP_LP_APPS_HOST as any,
  },
  testflight: {
    host: process.env.REACT_APP_TESTFLIGHT_HOST as any,
    token: process.env.REACT_APP_TESTFLIGHT_TOKEN as any,
  },
  applyUrl: 'https://www.lendingpoint.com/apply/#/Start',
  contactUrl: 'https://www.lendingpoint.com/contact-us/',
  contactPhone: '+18889690959',
  tcUrl: 'https://www.lendingpoint.com/terms-of-use/',
  electronicCommunicationsUrl: 'https://www.lendingpoint.com/consent-electronic-communications/',
  login: {
    initialValues: {
      username: process.env.REACT_APP_DEBUG_USERNAME as any,
      password: process.env.REACT_APP_DEBUG_PASSWORD as any,
    },
  },
};

const API_HOST = config.api.host;

export { API_HOST };

export default config;
