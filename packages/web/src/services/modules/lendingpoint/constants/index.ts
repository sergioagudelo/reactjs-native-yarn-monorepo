export const NAME = 'LENDINGPOINT';

export const paths = {
  user: '/me',
  documents: '/me/documents',
  login: '/signin',
  loans: '/me/loans',
  mails: '/me/mails',
  resetPassword: '/accesses/send_reset_password_email',
  paymentMethods: '/me/payment-methods',
  loanApplications: '/me/loan-applications',
  apiManagerLogin: '/oauth/token',
  offersCatalog: '/offers/catalog',
  applications: '/applications',
} as const;

// errors handled by api
export const errorNames = {
  unauthorized: 'unauthorized',
  general: 'general',
  refreshToken: 'refreshToken',
  // for errors handled by module services
  unexpected: 'subModule',
};
