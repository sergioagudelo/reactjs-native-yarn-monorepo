import axios from 'axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import config from '@customer-portal/common/src/config';
// import NavigationService from '../../../../navigation/NavigationService';
// FIXME nested type
import type { LoginResponse } from '@customer-portal/common/src/modules/auth/utils';

import { httpErrorLogger } from '@customer-portal/common/src/services/middlewares';
import * as constants from '../constants';
import { handleError } from '@customer-portal/common/src/utils/services';
import type { AuthTokens } from '../utils';
import manager, { createApiManagerToken } from './manager';
import { useHistory } from 'react-router-dom';

const { paths, NAME, errorNames } = constants;
const {
  api: {
    host,
    apiManager: { appToken },
    sdocs,
    paths: { consumer: consumerPath },
  },
} = config;

let authTokens: AuthTokens | null = null;
let isRefreshingToken = false;

export const AUTH_TOKEN_ASYNC_STORAGE_KEY = 'auth.token';

const consumer = axios.create({
  baseURL: host + consumerPath,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export const getAuthTokens = () => authTokens;

export const clearAuthTokens = () => {
  authTokens = null;
  AsyncStorage.setItem(AUTH_TOKEN_ASYNC_STORAGE_KEY, JSON.stringify(null));

};

export const setAuthTokens = (data: LoginResponse) => {
  authTokens = {
    accessToken: data.authorizationToken,
    refreshToken: data.refreshToken,
    expiresIn: moment()
      .add(data.expires, 's')
      .subtract(60, 's')
      .toDate(),
  };
  AsyncStorage.setItem(AUTH_TOKEN_ASYNC_STORAGE_KEY, JSON.stringify(authTokens));
};

export const addAuthTokens = (data: any) => {
  if (authTokens) {
    authTokens = {
      ...authTokens,
      ...data,
    };
    AsyncStorage.setItem(AUTH_TOKEN_ASYNC_STORAGE_KEY, JSON.stringify(authTokens));
  }
};

export const refreshToken = async (): Promise<any> => {

  try {
    if (!authTokens || isRefreshingToken) {
      return false
    }
    isRefreshingToken = true;
    await createApiManagerToken()
    const response = await manager.post(paths.apiManagerLogin, {
      grantType: "refresh_token",
      refreshToken: authTokens.refreshToken,
    })
    const managerAuthorization = authTokens ? authTokens.managerAuthorization : null;
    setAuthTokens(response.data);
    if (managerAuthorization) {
      addAuthTokens({ managerAuthorization });
    }
    return true
  } catch (error) {
    httpErrorLogger(NAME, errorNames.refreshToken, error);
    clearAuthTokens();
    return false
  } finally {
    isRefreshingToken = false
  }
};
/** REQUEST INTERCEPTOR
 */
consumer.interceptors.request.use(
  async request => {
    if (authTokens || Object.keys(authTokens ?? {}).length === 0) {
      try {
        const authTokenString = await AsyncStorage.getItem(AUTH_TOKEN_ASYNC_STORAGE_KEY);
        if (authTokenString) {
          authTokens = JSON.parse(authTokenString);
        }
      } catch (e) {
        clearAuthTokens()
      }
    }

    if (authTokens) {
      if (moment().isAfter(authTokens.expiresIn)) {
        await refreshToken();
      }
      request.headers.Authorization = `Bearer ${authTokens.accessToken}`;

      // add sdocs credentials
      if (
        request.url &&
        request.url.startsWith(paths.applications) &&
        request.url.search(new RegExp(paths.offersCatalog, 'g')) < 0
      ) {
        request.headers.Authorization = authTokens.managerAuthorization;
        request.headers['Application-Token'] = sdocs.appToken;
      }

      // add manager auth credentials
      if (
        authTokens.managerAuthorization &&
        request.url === paths.applications + paths.offersCatalog
      ) {
        request.headers.Authorization = `Bearer ${authTokens.managerAuthorization}`;
        request.headers['Application-Token'] = appToken;
      }

      // console.log(request.headers);
    }

    return request;
  },
  (error) => {
    return Promise.reject(error);
  }
);

/**
 * API RESPONSE INTERCEPTOR
 */
consumer.interceptors.response.use(
  (response) => {
    if (response.status === 204) {
      return { ...response, data: 'Successful update' };
    }
    return response;
  },
  (error) => {
    const {
      response: { status },
    } = error;
    if (status === 401) {
      httpErrorLogger(NAME, errorNames.unauthorized, error);
      clearAuthTokens();
      // REV: web implementation
      // NavigationService.navigate('Auth');
      useHistory().push('/auth');

    } else if (status === 403 || status === 404 || status >= 500) {
      // general error
      httpErrorLogger(NAME, errorNames.general, error);
      // NavigationService.navigate('TryAgain', { isLogged: !!authTokens });
      // REV: missing web implementation
      useHistory().push('/TryAgain', { isLogged: !!authTokens });
    }

    // Log server errors in sentry
    if (status >= 500) {
      handleError(error);
    }

    return Promise.reject(error);
  }
);

export default consumer;
