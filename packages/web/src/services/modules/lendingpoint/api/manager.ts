import axios from 'axios';
import { useHistory } from 'react-router-dom';

import config from '@customer-portal/common/src/config';
// import NavigationService from '../../../../navigation/NavigationService';
// FIXME nested type

import { httpErrorLogger } from '@customer-portal/common/src/services/middlewares';
import { addAuthTokens, handleSubModuleError } from '../helpers'
import * as constants from '../constants';
import type { ManagerTokens } from '../utils';
import type { HTTPServicesResponse } from '@customer-portal/common/src/services/utils';

const { NAME, errorNames, paths } = constants;
const {
  api: {
    host,
    apiManager: { appToken, managerAuth },
    paths: { manager: managerPath },
  },
} = config;

let authTokens: ManagerTokens;

const manager = axios.create({
  baseURL: host + managerPath,
  headers: {
    'Content-Type': 'application/json',
  },
});

const clearAuthTokens = () => {
  authTokens = null;
};

export const setManagerTokens = (data: any) => {
  authTokens = {
    authorization: data.accessToken,
  };
  addAuthTokens({ managerAuthorization: data.accessToken });
};

export const createApiManagerToken = async (): Promise<
  HTTPServicesResponse<any>
> => {
  try {
    const payload = {
      grantType: 'password',
      ...managerAuth,
    };
    const response = await axios({
      method: 'post',
      url: `${host}${managerPath}${paths.apiManagerLogin}`,
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify(payload),
    });
    setManagerTokens(response.data)
    return { success: true, payload: 'No content' };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
/**
 * API REQUEST INTERCEPTOR
 */
manager.interceptors.request.use(
  async (request) => {
    if (authTokens) {
      request.headers.Authorization = `Bearer ${authTokens.authorization}`;
      request.headers['Application-Token'] = appToken;
    }
    return request;
  },
  (error) => Promise.reject(error)
);

/**
 * API RESPONSE INTERCEPTOR
 */
manager.interceptors.response.use(
  (response) => {
    if (response.status === 204) {
      return { ...response, data: 'Successful update' };
    }
    return response;
  },
  (error) => {
    const {
      response: { status },
    } = error;

    if (status === 401) {
      httpErrorLogger(NAME, errorNames.unauthorized, error);
      clearAuthTokens();
      // REV: web implementation
      // NavigationService.navigate('Auth');
      useHistory().push('/auth');
    } else if (status === 403 || status === 404 || status >= 500) {
      // general error
      httpErrorLogger(NAME, errorNames.general, error);
      // NavigationService.navigate('TryAgain', { isLogged: !!authTokens });
      useHistory().push('/TryAgain', { isLogged: !!authTokens });
    }
    return Promise.reject(error);
  }
);

export default manager;
