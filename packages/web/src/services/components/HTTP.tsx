import React from 'react';
import shared from '../../modules/shared';

import { useHTTP } from '../hooks';
import { HTTPServicesResponse } from '../utils';

// const { Loading } = shared.components;

export type HTTPErrorMessageComponentProps = { errorMessage: string };
export type HTTPPayloadMessageComponentProps = { payload: any };

export type HTTPProps = {
  // REV: ERROR onSubmit changed for callOnComponentMount since onSubmit does not exists on useHTTP
  // onSubmit: () => Promise<HTTPServicesResponse>;
  callOnComponentMount: () => Promise<HTTPServicesResponse>;
  // REV: ERROR asyncFunction changed for httpFunction since asyncFuntion does not exists on useHTTP
  // asyncFunction: () => Promise<HTTPServicesResponse>;
  httpFunction: () => Promise<HTTPServicesResponse>;
  functionPayload?: any;
  LoadingComponent?: () => React.ReactElement<any>;
  ErrorMessageComponent?: ({
    errorMessage,
  }: HTTPErrorMessageComponentProps) => React.ReactElement<any>;
  handlePayload?: ({ payload }: HTTPPayloadMessageComponentProps) => React.ReactElement<any> | null;
};

export default function withHTTP(props: HTTPProps) {
  const {
    httpFunction,
    functionPayload,
    LoadingComponent,
    ErrorMessageComponent,
    handlePayload,
    callOnComponentMount,
  } = props;

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { isLoading, errorMessage, payload } = useHTTP({
    httpFunction,
    functionPayload,
    callOnComponentMount,
  });

  // TODO default components

  if (isLoading) {
    return LoadingComponent ? <LoadingComponent /> : ''; //<Loading isLoading />;
  }
  if (errorMessage) {
    return ErrorMessageComponent ? <ErrorMessageComponent errorMessage={errorMessage} /> : null;
  }
  if (payload) {
    return handlePayload ? handlePayload({ payload }) : null;
  }
  return null;
}
