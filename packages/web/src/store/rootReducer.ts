import { combineReducers } from 'redux';

import { Action } from 'typesafe-actions';
import shared from '../modules/shared';
import loans from '../modules/loans';
import payments from '../modules/payments';
import profile from '../modules/profile';
import auth from '../modules/auth';
// import notifications from '../modules/notifications';
// import refinance from '../modules/refinance';
// REV: support for web projects
import AsyncStorage from '@react-native-community/async-storage';

import { actions as authActions } from '../modules/auth/store/actions';
import { lendingpoint } from '../services';

const reducer = combineReducers({
  [shared.constants.NAME]: shared.store.reducer,
  [loans.constants.NAME]: loans.store.reducer,
  [payments.constants.NAME]: payments.store.reducer,
  [profile.constants.NAME]: profile.store.reducer,
  [auth.constants.NAME]: auth.store.reducer,
  // [notifications.constants.NAME]: notifications.store.reducer,
  // [refinance.constants.NAME]: refinance.store.reducer,
});

export default (state: any, action: Action) => {
  // TODO make this dynamic using withe list
  // RESET STORAGE
  if (action.type === authActions.logout().type) {
    const {
      biometricAuthPermission,
      bioAuthPromptRejected,
      isBiometricAuthSupported,
      appLocalVersion,
    } = state.shared.device;

    state = {
      shared: {
        device: {
          biometricAuthPermission,
          bioAuthPromptRejected,
          isBiometricAuthSupported,
          appLocalVersion,
        },
      },
      loans: {
        list: null,
        details: null
      },
      profile: {
        userInfo: null
      },
      payments: {
        methods: null
      },
      auth: {
        isAuthLoading: false
      },
      notifications: {
        list: null
      },
      refinance: {
        opportunity: null,
        opportunityError: "",
        offers: null,
        tila: null,
        selectedOffer: null
      }
    };
    // REV: can be implemented
    lendingpoint.helpers.clearAuthTokens();
    AsyncStorage.clear();
  }
  // DELEGATE REDUCER
  return reducer(state, action);
};
