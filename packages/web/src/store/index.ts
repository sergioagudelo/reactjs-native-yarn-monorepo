// ? Redux + Typescript @see https://joshuaavalon.io/create-type-safe-react-redux-store-with-typescript
import { applyMiddleware, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

// import Reactotron from '../config/ReactotronConfig';

import rootReducer from './rootReducer';

const store = createStore(
  persistReducer({ key: 'root', storage: AsyncStorage }, rootReducer),
  composeWithDevTools(
    applyMiddleware(thunk),
    // Reactotron.createEnhancer(),
  ),
);

const persistor = persistStore(store);

export { store, persistor };
