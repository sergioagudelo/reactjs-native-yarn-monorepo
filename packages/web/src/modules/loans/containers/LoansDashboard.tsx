import React, { useState, useEffect } from 'react';
// import { SafeAreaView } from 'react-native';
// import { Text, ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
// import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// store
import { RootState } from 'typesafe-actions';
// import { withNavigation } from 'react-navigation';
// import { NavigationStackScreenProps } from 'react-navigation-stack';
import * as store from '../store';
import { getUserInfo } from '../../auth/store/selectors';

// REV: mobile,
// import { keychain } from '../../auth/services';

// Modules
import shared from '../../shared';

// Components
// REV: mobile,
// import { LoansDashboardComponent, AllowBiometricAuthModal } from '../components';
import { LoansDashboardComponent } from '../components';
import Loading from 'modules/loading/Loading';

// REV: mobile,
// REV: webkitCancelAnimationFrame, Loading web implementation
// const { Loading } = shared.components;
const { store: sharedStore } = shared;

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getAllLoans: store.actions.thunks.getAll,
      setBiometricAuthPermission: sharedStore.actions.actions.setBiometricAuthPermission,
      setModalShown: sharedStore.actions.actions.setModalShown,
      setIsBiometricAuthSupported: sharedStore.actions.actions.isBiometricAuthSupported,
      setBioAuthPromptRejected: sharedStore.actions.actions.setBioAuthPromptRejected,

    },
    dispatch,
  );

const mapStateToProps = (state: RootState) => ({
  loansList: store.selectors.getLoans(state),
  navigationHistory: sharedStore.selectors.getNavigationHistory(state),
  // biometricAuthPermission: sharedStore.selectors.getBiometricAuthPermission(state),
  // isBioAuthModalShown: sharedStore.selectors.getModalShown(state),
  // bioAuthPromptRejected: sharedStore.selectors.getIsBioAuthPromptRejected(state),
  userInfo: getUserInfo(state),
});

type LoansProps =
  // ThemedComponentProps &
  // NavigationStackScreenProps &
  ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

const LoansDashboard = ({
  getAllLoans,
  loansList,
  // isBioAuthModalShown,
  setModalShown,
  // biometricAuthPermission,
  setBiometricAuthPermission,
  navigationHistory,
  setIsBiometricAuthSupported,
  // bioAuthPromptRejected,
  setBioAuthPromptRejected,
  // theme,
  userInfo
}: LoansProps) => {

  const [isBioAuthModalVisible, setIsBioAuthModalVisible] = useState(false);

  const [bioType, setBioType] = useState('');
  const [failed, setFailed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const onDismissModalHandler = () => {
    setBioAuthPromptRejected(true);
    setIsBioAuthModalVisible(false);
    setModalShown(true);
  };

  useEffect(() => {
    const getLoans = async () => {
      setIsLoading(true);
      await getAllLoans();
      setIsLoading(false);

      // setIsLoading(false);
      // setFailed(true);
    };

    if (!loansList && !isLoading && !failed && userInfo) {
      getLoans();
    }

    // REV: mobile, biometric authentication
    // const checkBiometrySupport = async () => {
    //   const { success, payload } = await keychain.checkBiometrySupport();
    //   if (success) {
    //     setBioType(payload);
    //     setIsBiometricAuthSupported(true);
    //     setIsBioAuthModalVisible(true);
    //   }
    // };

    // REV: mobile, biometric authentication
    // if (
    //   navigationHistory.previousRouteName === 'Login' &&
    //   !biometricAuthPermission &&
    //   !isBioAuthModalShown
    // ) {
    //   checkBiometrySupport();
    // }
  }, [
    failed,
    isLoading,
    getAllLoans,
    loansList,
    // biometricAuthPermission,
    navigationHistory.previousRouteName,
    isBioAuthModalVisible,
    // isBioAuthModalShown,
    setIsBiometricAuthSupported,
  ]);

  if (failed) {
    // navigator.navigate // TODO redirect to general error
    return (
      <p>FAILED LOANS FETCHING TRY AGAIN</p>
    );
  }

  if (isLoading) {
    return (
      <Loading />
    );
  }

  return (
    <>
      {/* REV: mobile, biometric authentication modal */}
      {/* {isBioAuthModalVisible && (
        <AllowBiometricAuthModal
          bioType={bioType}
          visible={!bioAuthPromptRejected && isBioAuthModalVisible}
          onDismiss={onDismissModalHandler}
          onSubmit={() => setBiometricAuthPermission(true)}
        />
      )} */}
      {loansList && <LoansDashboardComponent loans={loansList} />}
    </>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoansDashboard);
