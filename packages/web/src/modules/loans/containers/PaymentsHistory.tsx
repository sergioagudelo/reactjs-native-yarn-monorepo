import React, { useState, useCallback, useEffect } from 'react';
import { compose, Dispatch, bindActionCreators } from 'redux';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';

// Components
import { PaymentsComponent } from '../components';

// Constants
// REV: movil
// import { selectPaymentOptions } from '../constants';

// Types
import type { RootState } from 'typesafe-actions';
import type { LoanDetailsType, PaymentHistoryItemType } from '@customer-portal/common/src/modules/loans/utils';

// Modules
import * as loansService from '../services/loans';
import { paymentHistoryParser } from '@customer-portal/common/src/modules/loans/helpers';

// REV: movil
// type LoanDetailsMapStateProps = {
//   navigation: NavigationStackProp;
// };

// Store
import * as store from '../store';

type LoanDetailsMapStateProps = {
  loanId: string;
};

const mapStateToProps = (state: RootState, { loanId }: LoanDetailsMapStateProps) => ({
  loan: store.selectors.getLoanById(state, loanId),
  loanDetails: store.selectors.getLoanDetails(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getLoanDetails: store.actions.thunks.getDetails,
    },
    dispatch,
  );

type LoanDetailsConnectProps = { loanDetails: LoanDetailsType } &
  LoanDetailsMapStateProps &
  ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const PaymentsHistory = ({ loan, loanId, loanDetails }: LoanDetailsConnectProps
) => {
  useEffect(() => {
    if (!loan) {
      return;
    }
  }, [loan]);

  const [loading, setLoading] = useState(false);
  const [failed, setFailed] = useState(false);
  const [payments, setPayments] = useState<PaymentHistoryItemType[]>([]);
  const getPayments = useCallback(async (id: string) => {
    setLoading(true);
    const result = await loansService.getPaymentHistory(id, { days: 60, offset: 0, limit: 5 });
    if (result.success && result.response) {
      const parsedPayments = paymentHistoryParser(result.response);
      setPayments(prev => [...prev, ...parsedPayments]);
      setLoading(false);
      return;
    }
    setFailed(true);
    setLoading(false);
  }, []);

  return (
    <PaymentsComponent
      getPayments={getPayments}
      loanId={loanId}
      payments={payments}
      isLoading={loading}
      failed={failed}
      loan={loan}
      loanDetails={loanDetails}
    />
  );
};

// Rev: movil
// export default compose(withNavigation)(PaymentsHistory);

export default compose(
  // withNavigation,
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(PaymentsHistory) as React.ElementType;

