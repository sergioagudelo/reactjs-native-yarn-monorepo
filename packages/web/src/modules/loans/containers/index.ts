import PaymentsHistory from './PaymentsHistory';
import LoansDashboard from './LoansDashboard';
import LoanDetailsContainer from './LoanDetailsContainer';
// import SetUpAutoPayContainer from './SetUpAutoPay';

// export { PaymentsHistory, LoansDashboard, LoanDetails, SetUpAutoPayContainer };
export { LoansDashboard, LoanDetailsContainer, PaymentsHistory };
