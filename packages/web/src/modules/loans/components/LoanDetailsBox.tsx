import React, { ReactNode } from 'react';

type LoanDetailsBoxProps = {
  title: string;
  children?: ReactNode;
};

const LoanDetailsBox: React.FC<LoanDetailsBoxProps> = ({
  title,
  children,
}: LoanDetailsBoxProps) => {
  return (
    <div className="uk-card uk-card-default lcp-details">
      <div className="uk-card-header uk-background-muted">
        <h2 className="uk-text-lead uk-text-bluedark uk-text-center uk-margin-small">{title}</h2>
      </div>
      <div className="uk-padding-small">
        {children}
      </div>
    </div>
  );
};

export default LoanDetailsBox;
