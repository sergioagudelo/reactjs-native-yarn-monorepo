import React, { useCallback, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

// Types
import { Loan, LoanDetailsType } from '../utils';

// Components
import { ExtraPaymentContainer } from '../../payments/containers';
import LoanAutoPay from './LoanAutoPay';
import LoanNoAutoPay from './LoanNoAutoPay';
import Loading from 'modules/loading/Loading';
import LoanMoreDetails from './LoanMoreDetails';
import LoanBalanceHeader from './LoanBalanceHeader';
import { PaymentHistoryItemType } from '@customer-portal/common/src/modules/loans/utils';

// Modules
import * as loansService from '../services/loans';
import { paymentHistoryParser } from '@customer-portal/common/src/modules/loans/helpers';
import { formatDate } from '@customer-portal/common/src/modules/shared/helpers/format';
import shared from '@customer-portal/common/src/modules/shared';

// Images
import icoOkGreen from 'images/icons/ico-ok-green.svg';

type LoanDetailsComponentProps = {
  loan: Loan;
  loanDetails: LoanDetailsType;
  isLoading: boolean;
};

const LoanDetailsComponent = ({
  loan,
  loanDetails,
  isLoading,
}: LoanDetailsComponentProps) => {
  let history = useHistory();
  let location = useLocation().pathname;

  const [loadingPaymentHistory, setLoadingPaymentHistory] = useState(false);
  const [failedPaymentHistory, setFailedPaymentHistory] = useState(false);
  const [payments, setPayments] = useState<PaymentHistoryItemType[]>([]);

  useEffect(() => {
    getPayments(loan.loanId)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Get payments history sor subcomponent payment history.
  const getPayments = useCallback(async (id: string) => {
    setLoadingPaymentHistory(true);
    const result = await loansService.getPaymentHistory(id, { days: 60, offset: 0, limit: 5 });
    if (result.success && result.response) {

      const parsedPayments = paymentHistoryParser(result.response);
      setPayments(prev => [...prev, ...parsedPayments]);
      setLoadingPaymentHistory(false);
      return;
    }
    setFailedPaymentHistory(true);
    setLoadingPaymentHistory(false);
  }, []);

  return (
    <React.Fragment>
      <LoanBalanceHeader
        lai={loan.lai}
        balance={loan.balance}
        loanDetails={loanDetails}
      />

      {!isLoading && loanDetails ? (
        <form className="uk-form-stacked">
          <div className="uk-container uk-container-small lcp-loan-payment-details">
            <div className="uk-grid-collapse uk-grid-match uk-flex uk-flex-center uk-flex-middle uk-flex-row uk-card uk-card-default" uk-grid="true">
              {loanDetails.isAutopay ? (
                <LoanAutoPay loan={loan} details={loanDetails} />
              ) : (
                  <LoanNoAutoPay loan={loan} />
                )}
            </div>
            <div className="uk-container uk-container-small lcp-loan-details">
              <div className="uk-grid-collapse" uk-grid="true">
                {!loanDetails.overDueDays && <ExtraPaymentContainer loan={loan} />}
                <LoanMoreDetails loanId={loan.loanId} loanDetails={loanDetails} />

                {loadingPaymentHistory ?
                  (<Loading />) :
                  failedPaymentHistory ?
                    (<p>Something went wrong!</p>) :
                    (
                      <div className="uk-width-1-2@s">
                        <div className="uk-card uk-card-body lcp-payment-history">
                          <h2 className="uk-text-lead uk-text-bluedark uk-text-center uk-margin-small">Latest Payments</h2>
                          <hr className="uk-divider-blue uk-margin-small uk-margin-remove-bottom" />
                          <div className="uk-flex uk-flex-middle lcp-loan-info">
                            <table className="uk-table uk-table-divider uk-table-hover uk-table-middle">
                              <tbody>
                                {payments ?
                                  payments.map((payment: PaymentHistoryItemType, index: number) => {
                                    return (
                                      <tr key={index}>
                                        <td>
                                          <p className="uk-margin-remove uk-text-bluedark">${payment.payment} on {' '}
                                            {formatDate(
                                              payment.paymentDate,
                                              shared.utils.formatDateOptions[0],
                                            )}
                                          </p>
                                          <small className="uk-text-small uk-text-uppercase uk-text-graydark uk-text-bolder">#{payment.lpt} • ACH</small>
                                        </td>
                                        <td className="uk-table-link"><img className="uk-preserve-width" src={icoOkGreen} alt="icoOkGreen" /></td>
                                      </tr>
                                    )
                                  })
                                  : (
                                    <p>There are not payments to show.</p>
                                  )}
                              </tbody>
                            </table>
                          </div>
                          <div className="uk-text-center">
                            <button
                              className="uk-button uk-button-secondary uk-width-small uk-margin-auto"
                              onClick={() => history.push(`${location}/paymentsHistory`, { "loanId": loan?.loanId })}
                            >
                              View All
                      </button>
                          </div>
                        </div>
                      </div>
                    )
                }

              </div>
            </div>
          </div>
        </form>
      ) : (
          <Loading />
        )
      }
    </React.Fragment >
  );
};

export default LoanDetailsComponent;
