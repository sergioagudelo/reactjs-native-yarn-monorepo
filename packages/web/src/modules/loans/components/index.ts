import LoansDashboardComponent from './LoansDashboardComponent';
import LoanCard from './LoanCard';
// import NewLoan from './NewLoan';
// import BgArrow from './BgArrow';
import LoanDetailsComponent from './LoanDetailsComponent';
import LoanMoreDetails from './LoanMoreDetails';
import LoanAutoPay from './LoanAutoPay';
import LoanNoAutoPay from './LoanNoAutoPay';
import SetUpAutoPay from './SetUpAutoPay';
// import PaymentModalComponent from './PaymentModal';
// import PaymentComponent from './Payment';
import PaymentsComponent from './Payments';
// import VirtualCardActivate from './VirtualCardActivate';
// import NewLoanOfferCard from './NewLoanOfferCard';
// import OfferCardItem from './OfferCardItem';
// import NiceToSeeYou from './NiceToSeeYou';
import LoanBalanceHeader from './LoanBalanceHeader';
// import AllowBiometricAuthModal from './AllowBiometricAuthModal';
import LoanDetailsBox from './LoanDetailsBox';

export {
  LoanBalanceHeader,
  //   NiceToSeeYou,
  LoansDashboardComponent,
  //   NewLoan,
  LoanCard,
  //   BgArrow,
  LoanDetailsComponent,
  LoanMoreDetails,
  LoanAutoPay,
  LoanNoAutoPay,
  SetUpAutoPay,
  PaymentsComponent,
  //   PaymentComponent,
  //   PaymentModalComponent,
  //   VirtualCardActivate,
  //   NewLoanOfferCard,
  //   OfferCardItem,
  //   AllowBiometricAuthModal,
  LoanDetailsBox,
};
