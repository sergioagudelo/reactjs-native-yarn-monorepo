import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';

// Type
import type { Loan } from '../utils';

// Modules
import shared from '../../shared';

// Styles & Images
import icoPaymentDate from 'images/icons/ico-payment-date.svg';

type LoanNoAutoPayComponentProps = {
  loan: Loan;
};

const LoanNoAutoPay = ({
  loan,
}: LoanNoAutoPayComponentProps) => {
  let history = useHistory();
  let location = useLocation().pathname;
  let date = shared.helpers.format.formatDate(
    loan.paymentDate,
    shared.utils.formatDateOptions[1],
  ).split(',');

  return (
    <React.Fragment>
      <div className="lcp-payment">
        <div className="uk-card uk-card-body uk-flex-left uk-flex-top uk-flex">
          <img className="uk-margin-small-right" src={icoPaymentDate} alt="" />
          <div className="uk-text-left">
            <p className="uk-text-meta uk-margin-remove uk-text-uppercase">
              {loan.overDueDays ? 'PAYMENT WAS DUE ON' : 'NEXT PAYMENT DUE'}
            </p>
            <p className="uk-margin-remove uk-text-blue">
              {date && date[0]}<br />
              {date && `${date[1]}, ${date[2]}`}
            </p>
          </div>
        </div>
      </div>
      <div className="lcp-payment">
        <div className="uk-card uk-card-body uk-text-center">
          <p className="uk-text-meta uk-margin-remove uk-text-uppercase">Payment Amount</p>
          <p className="uk-text-number uk-margin-remove uk-text-bluelight">
            {shared.helpers.format.formatMoney(loan.payment, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
            {loan.paymentDate}
          </p>
        </div>
      </div>
      <div className="lcp-payment">
        <div className="uk-card uk-card-body">
          <button
            className="uk-button uk-button-primary uk-width-small"
            onClick={() => history.push(`${location}/MakePayment`, { loanId: loan.loanId })}
          >
            Pay Now
          </button>
        </div>
      </div>
    </React.Fragment>
  );
}

export default LoanNoAutoPay;
