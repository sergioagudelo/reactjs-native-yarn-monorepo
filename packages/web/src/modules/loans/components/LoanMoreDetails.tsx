import React from 'react';

// Types
import { LoanDetailsType } from '../utils';

// Modules
import shared from '../../shared';

// Components
import SetUpAutoPayContainer from '../containers/SetUpAutoPay';
import LoanDetailsBox from './LoanDetailsBox';

type DetailItemComponentProps = {
  label: string;
  content: string;
};

const DetailItem = ({
  label,
  content,
}: DetailItemComponentProps) => (
    <div className="uk-flex uk-flex-middle lcp-loan-info uk-padding-small">
      <small className="uk-width-1-2 uk-text-right uk-text-small uk-text-graydark uk-text-uppercase uk-margin-small-right">{label}</small>
      <strong className="uk-width-1-2 uk-text-left uk-text-medium uk-text-bluedark">{content}</strong>
    </div>
  );

type LoanMoreDetailsComponentProps = {
  loanId: string;
  loanDetails: LoanDetailsType;
};

const LoanMoreDetails = ({
  loanId,
  loanDetails,
}: LoanMoreDetailsComponentProps) => {
  return (
    <div className="uk-width-1-2@s">

      <LoanDetailsBox title="More Details">
        <DetailItem
          label="LOAN AMOUNT"
          content={shared.helpers.format.formatMoney(loanDetails.disbursalAmount, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
        />
        <DetailItem
          label="PRINCIPAL PAID"
          content={shared.helpers.format.formatMoney(loanDetails.principalPaid, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
        />
        <DetailItem
          label="INTEREST PAID"
          content={shared.helpers.format.formatMoney(loanDetails.interestPaid, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
        />
      </LoanDetailsBox>
      <LoanDetailsBox title="Autopay Settings">
        <SetUpAutoPayContainer loanId={loanId} loanDetails={loanDetails} />
      </LoanDetailsBox>

    </div>
  );
};

export default LoanMoreDetails;
