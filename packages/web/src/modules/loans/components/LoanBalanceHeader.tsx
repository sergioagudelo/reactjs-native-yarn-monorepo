import { LoanDetailsType } from '@customer-portal/common/src/modules/loans/utils';
import React from 'react';

import shared from '../../shared';

type LoanBalanceHeaderProps = {
  lai: string | undefined;
  balance?: number;
  loanDetails?: LoanDetailsType;
  theme?: string,
};

const LoanBalanceHeader = ({ lai, balance, loanDetails, theme = "blue" }: LoanBalanceHeaderProps) => {
  const fontTheme = theme === 'blue' ? ['white', 'white'] : ['graydark', 'blue'];

  return (
    <div className="uk-container uk-container-small lcp-section-head uk-margin-medium-bottom">
      <div className="lcp-loan-info uk-flex uk-flex-middle uk-flex-center">
        <div className="uk-width-1-2@s lcp-info-left uk-flex uk-flex-middle uk-flex-right">
          <div className={`uk-text-uppercase uk-text-${fontTheme[0]} uk-text-bolder uk-text-right@s uk-text-center`}>
            <small>{lai}</small>
            {balance ? (
              <p className={`uk-text-meta uk-text-${fontTheme[1]} uk-margin-remove`}>Loan Balance</p>
            ) : null}
          </div>
          {balance ? (
            <div className={`uk-text-${fontTheme[1]} uk-text-number uk-margin-small-left`}>
              {shared.helpers.format.formatMoney(balance, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
            </div>
          ) : null}
        </div>
        {loanDetails && loanDetails.lastPaymentAmount && loanDetails.lastPaymentDate &&
          <div className={`uk-width-1-2@s lcp-info-right uk-text-meta uk-text-${fontTheme[1]} uk-text-uppercase uk-text-center`}>

            {`Last payment ${shared.helpers.format.formatMoney(loanDetails.lastPaymentAmount, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}`}
            {` on Sept ${shared.helpers.format.formatDate(
              loanDetails.lastPaymentDate,
              shared.utils.formatDateOptions[0]
            )}`}
          </div>}
      </div>
    </div>
  );
};

export default LoanBalanceHeader;