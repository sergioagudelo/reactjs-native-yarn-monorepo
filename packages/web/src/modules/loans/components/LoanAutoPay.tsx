import React from 'react';

// Type
import type { Loan, LoanDetailsType } from '../utils';

// Modules
import shared from '../../shared';

// Components
import Loading from 'modules/loading/Loading';

// Styles & Images
import icoPaymentDate from 'images/icons/ico-payment-date.svg';

type LoanAutoPayComponentProps = {
  loan: Loan;
  details: LoanDetailsType;
};

const LoanAutoPay = ({ loan, details }: LoanAutoPayComponentProps) => {
  const autopayPaymentMethod = details.isAutopay
    ? shared.helpers.format.getLoanAutopayPaymentMethod(details)
    : null;
  return (
    <React.Fragment>
      <div className="lcp-payment">
        <div className="uk-card uk-card-body uk-flex-left uk-flex-top uk-flex">
          <img className="uk-margin-small-right" src={icoPaymentDate} alt="icoPaymentDate" />
          <div className="uk-text-left">
            <p className="uk-text-meta uk-margin-remove uk-text-uppercase">
              {loan.overDueDays ? 'PAYMENT WAS DUE ON' : 'NEXT PAYMENT DUE'}
            </p>
            <p className="uk-margin-remove uk-text-blue">
              {!loan.overDueDays && (
                'will be automatically'
              )}
              <br />
              {`${loan.overDueDays ? '' : 'withdrawn on '}${shared.helpers.format.formatDate(
                loan.paymentDate,
                shared.utils.formatDateOptions[0],
              )}`}
            </p>
          </div>
        </div>
      </div>
      {!autopayPaymentMethod ? (
        <Loading />
      ) : (
          <React.Fragment>
            <div className="lcp-payment">
              <div className="uk-card uk-card-body">
                <p className="uk-text-meta uk-margin-remove uk-text-uppercase">Payment Method</p>
                <p className="uk-margin-remove uk-text-blue">
                  {autopayPaymentMethod && autopayPaymentMethod.name}<br />
                  {autopayPaymentMethod && autopayPaymentMethod.number}
                </p>
              </div>
            </div>
          </React.Fragment>
        )}
      <div className="lcp-payment">
        <div className="uk-card uk-card-body uk-text-center">
          <p className="uk-text-meta uk-margin-remove uk-text-uppercase">Payment Amount</p>
          <p className="uk-text-number uk-margin-remove uk-text-bluelight">
            {shared.helpers.format.formatMoney(loan.payment, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          </p>
        </div>
      </div>
    </React.Fragment>
  );
}

export default LoanAutoPay;
