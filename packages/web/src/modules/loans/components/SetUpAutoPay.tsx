import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';

// Types
import type { LoanDetailsType } from '@customer-portal/common/src/modules/loans/utils';
import type { PaymentMethodType } from '@customer-portal/common/src/modules/payments/utils';

// Modules
import shared from '../../shared';
import Loading from 'modules/loading/Loading';

// Styles & Images
import icoEdit from 'images/icons/ico-edit.svg';

type SetUpAutoPayProps = {
  isLoading: boolean;
  loanDetails: LoanDetailsType;
  selectedPayment?: PaymentMethodType;
  paymentMethods: PaymentMethodType[];
};

const SetUpAutoPay = ({
  isLoading,
  loanDetails,
  selectedPayment,
  paymentMethods,
}: SetUpAutoPayProps) => {
  let history = useHistory();
  let location = useLocation().pathname;
  const isAutopayClass = loanDetails.isAutopay ? 'uk-label-active' : 'uk-label-disabled';

  const autopayPaymentMethod = loanDetails.isAutopay
    ? shared.helpers.format.getLoanAutopayPaymentMethod(loanDetails)
    : null;

  const handleSetupAutopay = () => {
    history.push(`${location}/autopaySetup`, { loanId: loanDetails.loanId });
  };

  const renderChangeButton = () => (
    <div className="uk-width-1-2 uk-text-right uk-text-medium uk-text-bluedark">
      <button
        className="buttonEditPaymentMethod"
        onClick={handleSetupAutopay}
        disabled={
          loanDetails.isAutopay && paymentMethods && paymentMethods.length === 1
        }
      >
        <img className="uk-margin-small-right" src={icoEdit} alt="icoEdit" />
      </button>
    </div>
  );

  return (
    <div className="uk-card-body lcp-autopay-setup">
      {isLoading && !paymentMethods ? (
        <Loading />
      ) : (
          <React.Fragment>
            <div className="uk-flex uk-flex-middle lcp-loan-info">
              <div className="uk-width-1-2 uk-text-left uk-text-small uk-text-graydarkx uk-text-uppercase uk-margin-small-right">
                Autopay Option
              </div>
              <div className="uk-width-1-2 uk-text-right uk-text-medium uk-text-bluedark">
                <span className={'uk-label uk-label-pill ' + isAutopayClass}>
                  {loanDetails.isAutopay ? `Active` : `Not enabled`}
                </span>
              </div>
            </div>
            <hr className="uk-divider-gray" />
            {loanDetails.isAutopay ? (
              <React.Fragment>
                <div className="uk-flex uk-flex-middle lcp-loan-info uk-margin">
                  <div className="uk-width-1-2 uk-text-left">
                    <p className="uk-margin-small uk-text-small uk-text-graydarkx uk-text-uppercase">
                      {
                        autopayPaymentMethod
                          ? 'Payment Method'
                          : 'Select a payment method'
                      }
                    </p>
                    <p className="uk-margin-remove uk-text-blue">
                      {
                        loanDetails.isAutopay && autopayPaymentMethod
                          ? `${autopayPaymentMethod.name} ${autopayPaymentMethod.number}`
                          : shared.helpers.format.formatPaymentMethodNumber(
                            selectedPayment
                          )
                      }
                    </p>
                  </div>
                  {renderChangeButton()}
                </div>
              </React.Fragment>
            ) : (
                <div className="uk-width-1-1 uk-text-center uk-text-bluedark">
                  <p>Setup autopay now and don’t worry for due dates anymore!</p>
                  <button
                    className="uk-button uk-button-secondary"
                    onClick={handleSetupAutopay}
                  >Setup Autopay</button>
                </div>
              )}

          </React.Fragment>
        )
      }
    </div>
  );
};

export default SetUpAutoPay;