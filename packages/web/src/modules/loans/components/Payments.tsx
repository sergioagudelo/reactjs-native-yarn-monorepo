import React, { useState } from 'react'
import VizSensor from 'react-visibility-sensor';
import { useLocation } from 'react-router-dom';

// Images
import imgPrint from 'images/icons/ico-print-white.svg'

// Types
import type { Loan, LoanDetailsType, PaymentHistoryItemType } from '@customer-portal/common/src/modules/loans/utils';

// Components & Modules
import { formatDate } from '@customer-portal/common/src/modules/shared/helpers/format';
import shared from '@customer-portal/common/src/modules/shared';
import LoanBalanceHeader from './LoanBalanceHeader';
import Loading from 'modules/loading/Loading';

/**
 * component type
 */
export type PaymentsComponentProps = {
  payments: PaymentHistoryItemType[] | undefined;
  modalData?: PaymentHistoryItemType;
  isLoading: boolean;
  failed: boolean;
  getPayments: (id: string) => Promise<void>
  loanId: string,
  loan: Loan | undefined,
  loanDetails: LoanDetailsType,
};

const PaymentsComponent = ({ payments, getPayments, loanId, loan, loanDetails }: PaymentsComponentProps) => {
  const [counter, setCounter] = useState(0);

  const handleLoadingMore = (isVisible: any) => {
    setCounter(counter + 1);
    if (isVisible && counter < 5) getPayments(loanId);
  }
  let location = useLocation();

  return (
    <div className="lcp-container uk-background-gray" uk-height-viewport="expand: true" >
      <LoanBalanceHeader lai={loan?.lai} balance={loan?.balance} loanDetails={loanDetails} theme="gray" />
      <form id="formHeight" className="uk-form-stacked">

        <div className="uk-container uk-container-small lcp-loan-payment-history">
          <div className="uk-card uk-card-default uk-card-body">
            <div className="lcp-filters uk-flex uk-flex-center uk-flex-top uk-grid-collapse" uk-grid="true">
              <div>
                <div uk-form-custom="target: > * > span:first-child">
                  <select>
                    <option value="">Select months</option>
                    <option value="1">Last 3 Months</option>
                    <option value="2">Last 6 Months</option>
                  </select>
                  <button className="uk-button uk-button-default" type="button" tabIndex={-1}>
                    <span></span>
                    <span uk-icon="icon: chevron-down"></span>
                  </button>
                </div>
              </div>
              <div>
                <button className="uk-button uk-button-blue" type="button" tabIndex={-1} onClick={() => window.print()}>
                  <span><img src={imgPrint} alt="imgPrint" /></span> Print
                  </button>
              </div>
            </div>
            <hr className="uk-divider-blue uk-margin-remove-bottom" />

            <table className="uk-table uk-table-divider uk-table-striped uk-table-hover uk-table-middle uk-margin-remove">
              <tbody>
                {
                  payments &&
                  payments.map((payment: PaymentHistoryItemType, index: number) => {
                    return (
                      <tr key={index} >
                        <td>
                          <small className="uk-text-small uk-text-uppercase uk-text-graydark">{payment.lpt}</small>
                          <p className="uk-margin-remove uk-text-bluedark">
                            {formatDate(
                              payment.paymentDate,
                              shared.utils.formatDateOptions[0],
                            )}
                          </p>
                        </td>
                        <td>
                          <small className="uk-text-small uk-text-uppercase uk-text-graydark">Amount</small>
                          <p className="uk-margin-remove uk-text-bluedark">{payment.payment}</p>
                        </td>
                        <td>
                          <small className="uk-text-small uk-text-uppercase uk-text-graydark">Principal</small>
                          <p className="uk-margin-remove uk-text-bluedark">{payment.principal}</p>
                        </td>
                        <td>
                          <small className="uk-text-small uk-text-uppercase uk-text-graydark">Interest</small>
                          <p className="uk-margin-remove uk-text-bluedark">{payment.interest}</p>
                        </td>
                        <td>
                          <small className="uk-text-small uk-text-uppercase uk-text-graydark">Method</small>
                          <p className="uk-margin-remove uk-text-bluedark">{payment.method}</p>
                        </td>
                      </tr>
                    )
                  })

                }

              </tbody>
            </table>
            {counter < 6 &&
              <VizSensor onChange={(isVisible) => handleLoadingMore(isVisible)} >
                <Loading loadingText="Loading more" />
              </VizSensor>
            }
          </div>
        </div>
      </form>

      <div className="uk-text-center uk-padding-small">
        <button uk-totop="true" uk-scroll="true"></button>
      </div>

    </div >



  )
}

export default PaymentsComponent;