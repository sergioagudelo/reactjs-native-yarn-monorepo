import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
// import { ThemedComponentProps, withStyles, Text, List, Layout } from 'react-native-ui-kitten';
// import { NavigationStackProp } from 'react-navigation-stack';

// Types
// import { withNavigation } from 'react-navigation';
// import { ListRenderItemInfo } from 'react-native';
import { Loan } from '../utils';

// Components
import LoanCard from './LoanCard';
// import { Loading } from '../../shared/components';

/**
 * type Loans type
 */
type LoansListProps = {
  // navigation: NavigationStackProp;
  loans: Loan[];
};

/**
 * Render loans and cards
 */
const LoansDashboard = ({ loans }: LoansListProps) => {
  let history = useHistory();
  let location = useLocation().pathname;
  const emptyItem = () => (
    <p>You don't have any loan account.</p>
  );

  return (
    <React.Fragment>
      <div className="lcp-dashboard uk-grid-medium uk-child-width-1-3@m uk-child-width-1-2@s uk-grid-match uk-flex-center" uk-grid="true">
        {loans ?
          loans.map((loan, index) =>
            <LoanCard
              overdue={loan.overDue ? 'loan-overdue' : ''}
              autopay={loan.isAutopay ? 'loan-autopay' : ''}
              loan={loan}
              onPress={() => history.push(`${location}/loanDetails`, { loanId: loan.loanId })}
              key={index}
            />
          )
          : emptyItem()
        }
      </div>
    </React.Fragment>
  );
};

export default LoansDashboard;
