import React from 'react';

// Components
import shared from '../../shared';
import { Loan } from '../utils';

// Images
import logoInvisalign from 'images/logo-invisalign.png';
import icoCalendar from 'images/icons/ico-calendar.svg';
import icoPlusLoan from 'images/icons/ico-plus-loan.svg';
import icoWarningWhite from 'images/icons/ico-warning-white.svg';
import icoPayment from 'images/icons/ico-payment.svg';

type LoanCardProps = {
  overdue: 'loan-overdue' | '';
  autopay: 'loan-autopay' | '';
  loan: Loan;
  onPress: () => void;
};

const LoanCard = ({ overdue = '', autopay = '', loan, onPress }: LoanCardProps) => {

  const nonOverdueClass = overdue ? "" : "non-overdue";

  return (
    <div className={'lcp-loan ' + overdue + autopay}>
      <button
        className="uk-card uk-card-small uk-card-default uk-card-body uk-flex-top uk-grid-collapse"
        uk-grid="true"
        onClick={onPress}
      >
        <div className="uk-text-meta lcp-loan-id uk-width-1-1">
          <small><img src={logoInvisalign} alt="logo-invisalign" /></small>
          <small>{loan.lai} •{' '}
            {shared.helpers.format.formatMoney(loan.amount, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}</small>
        </div>
        <div className="uk-width-2-5 lcp-loan-balance">
          <small className="left-item">Today's Balance</small>
          <strong className="number uk-text-bluedark uk-align-left">
            {shared.helpers.format.formatMoney(loan.balance, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          </strong>
        </div>
        <div className="uk-width-2-5 lcp-loan-balance">
          <small className="left-item">Next Payment</small>
          <strong className="number uk-text-bluelight uk-align-left">
            {shared.helpers.format.formatMoney(loan.payment, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          </strong>
          {overdue && <small className="uk-text-red uk-margin-remove uk-text-bolder uk-align-left">Was due on</small>}
          {/* {!autopay && */}
          <span className="date uk-align-left">
            <img src={icoCalendar} alt="ico-calendar" />
            {shared.helpers.format.formatDate(
              loan.paymentDate,
              shared.utils.formatDateOptions[0],
            )}
          </span>
          {/* } */}
        </div>
        <div className="uk-width-1-5 uk-text-right lcp-loan-plus">
          <img src={icoPlusLoan} alt="ico-plus-loan" />
        </div>
        {/* {autopay && <div className="uk-width-1-1 lcp-nextpayment uk-flex uk-flex-left uk-flex-middle">
          <img src={icoPayment} alt="ico-payment" />
          <span className="uk-text-middle">{loan.bottomTxt && loan.bottomTxt.toUpperCase()}</span>
        </div>} */}
      </button>
      <div className={"uk-label uk-label-pill uk-text-center " + nonOverdueClass}>
        {overdue && <span className="uk-icon"><img src={icoWarningWhite} alt="ico-warning-white" /></span>}
        {loan.bottomTxt && loan.bottomTxt.toUpperCase()}
      </div>
    </div >
  );
};

export default LoanCard;