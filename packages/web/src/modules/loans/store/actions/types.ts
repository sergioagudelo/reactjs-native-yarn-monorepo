import { NAME } from '../../constants';

import shared from '../../../shared';

// export default shared.helpers.createActionTypes(NAME, ['SOME_TYPE']);
export default shared.helpers.createActionTypes(NAME, [
  'LOAN_DASHBOARD.ATTEMPT',
  'LOAN_DASHBOARD.SUCCESS',
  'LOAN_DASHBOARD.FAILURE',
  'LOAN_DETAILS.ATTEMPT',
  'LOAN_DETAILS.SUCCESS',
  'LOAN_DETAILS.FAILURE',
]);
