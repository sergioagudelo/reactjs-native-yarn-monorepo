import React from 'react';
import { useLocation } from 'react-router-dom';

// Components
import { PaymentsHistory } from '../containers';

// Styles
import '../styles/paymentsHistory.scss';


type LocationParams = {
  loanId?: string
}

const PaymentsHistoryElement = () => {

  const loanId = useLocation<LocationParams>().state || 'empty';

  return (
    <PaymentsHistory loanId={loanId} />
  );
};



export default PaymentsHistoryElement;