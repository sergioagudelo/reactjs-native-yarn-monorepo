import Loans from './Loans';
import LoanDetails from './LoanDetails';
import PaymentsHistoryElement from './PaymentsHistory';

// export { Loans, LoanDetails, PaymentsHistory };
export { Loans, LoanDetails, PaymentsHistoryElement };
