import React from 'react';
import { useLocation } from 'react-router-dom';

// Components
import { LoanDetailsContainer } from '../containers';

// Styles
import '../styles/loan-details.scss';
import '../styles/loan-details-autopay.scss';
import '../styles/loan-details-head.scss'
import '../styles/loan-details-more.scss';
import '../styles/loan-details-history.scss';

type LocationParams = {
  loanId?: string
}

const LoanDetails = () => {
  const loanId = useLocation<LocationParams>().state?.loanId || 'empty';

  return (
    <div className="lcp-container uk-background-blue" uk-height-viewport="expand: true">
      <LoanDetailsContainer loanId={loanId} />
    </div>
  );
}

export default LoanDetails;