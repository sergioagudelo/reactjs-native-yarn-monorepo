import React from 'react';

// Modules
// import refinance from '../../refinance';
// import shared from '../../shared';

// Styles & Images
import '../styles/dashboard.scss';
import lpVirtualcard from 'images/lp-virtualcard.png';
import icoRefinancing from 'images/icons/ico-refinancing.svg';

// Components
import { LoansDashboard } from '../containers';
// containers
// const { VirtualCard } = shared.containers;
// const { Dashboard: RefinanceDashboard } = refinance.containers;

const Loans = () => {

  return (
    <div className="lcp-container uk-background-primary bg-angle-top" uk-height-viewport="expand: true">
      <div className="uk-container">
        <LoansDashboard />
        {/* <div className="uk-light uk-width-xlarge uk-margin-auto uk-padding-large">
          <h2 className="uk-heading-line uk-text-center uk-margin-remove"><span>Just for you:</span></h2>
        </div>
        <div className="lcp-dashboard-options uk-grid-medium uk-child-width-auto uk-grid-match uk-flex-center" uk-grid="true">
          <div className="lcp-cta lcp-virtualcard">
            <button className="uk-card uk-card-small uk-card-body uk-flex-top uk-grid-collapse" uk-grid="true">
              <img src={lpVirtualcard} alt="lpVirtualcard" />
            </button>
          </div>
          <div className="lcp-cta lcp-refinance">
            <button className="uk-card uk-card-small uk-card-body uk-flex-top uk-flex-left uk-grid-collapse" uk-grid="true">
              <img src={icoRefinancing} alt="icoRefinancing" />
              <div className="uk-width-expand">
                <p className="uk-text-xlarge uk-text-light uk-text-blue uk-margin-small">Good news!</p>
                <p className="uk-text-small uk-text-graydarkxx uk-text-normal uk-text-uppercase uk-margin-small">Your on-time payments have earned you access to more money.</p>
                <div className="uk-badge">GET $4,000.00 More!</div>
              </div>
            </button>
          </div>
        </div> */}
      </div>
    </div >
  );
};

export default Loans;
