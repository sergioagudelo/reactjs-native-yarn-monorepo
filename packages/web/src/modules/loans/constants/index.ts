// import { SelectOptionType } from 'react-native-ui-kitten';

/**
 * redux key name
 */
export const NAME = 'loans';

// REV: web, not used on web project
/**
 * LoanType for danger loan or blue loan
 */
// export const LoanType = {
//   danger: {
//     border: 'borderDanger',
//     back: 'backDanger',
//   },
//   default: {
//     border: 'borderInfo',
//     back: 'backInfo',
//   },
// };

// REV: mobile, used to select payment history
// export const selectPaymentOptions: SelectOptionType[] = [
//   { text: 'Select ...' },
//   { text: 'Last 3 Months' },
//   { text: 'Last 6 Months' },
//   { text: '2019' },
//   { text: '2018' },
//   { text: '2017' },
//   { text: '2016' },
// ];
