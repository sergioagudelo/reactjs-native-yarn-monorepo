import * as testData from './testData';

export type RefinanceOptionType = {
  savings: number;
};

export type Loan = {
  loanId: string;
  lai: string;
  amount: number;
  balance: number;
  payment: number;
  paymentDate: string;
  image?: string;
  bottomTxt?: string;
  isAutopay: boolean;
  subSource?: string;
  overDue?: boolean;
  lastPaymentAmount: number;
  overDueDays: number;
  loanPaymentAmount: number;
  frequency: string;
  remainingTerm: number;
};

export type PaymentHistoryItemType = {
  methodNumber: string;
  payment: number;
  interest: number;
  principal: number;
  method: string;
  paymentDate: string;
  id: string;
  lpt: string;
};

export type OfferType = {
  id: string;
  amount: number;
  fee: number;
  payment: number;
  totalPayment: number;
  financeCharge: number;
  term: number;
  installments: number;
  apr: number;
};

export type NewLoanOfferType = OfferType;

export type ConsolidateOfferType = OfferType & {
  title: string;
  description: string;
  savings: number;
};

export type VirtualCardType = {
  card: string;
  status: string;
  valid: string;
  csv: string;
} & OfferType;

export type LoansResponse = {
  message: string;
  endOfDayProcessRunningFlag: boolean;
  loanAccounts: {
    contractId: string;
    contractName: string;
    loanPayOffAmountAsOfToday: number; // loan balance
    loanStatus: string;
    loanPaymentAmount: number;
    isACH: boolean;
    isRefinance: boolean;
    paymentPassedDueDate: string;
    onlinePaymentFlag: boolean;
    currentLoanPaymentAmount: number;
    overDueDays: number;
    disbursalAmount?: number;
    subSource?: string;
    isAutopay?: boolean;
    nextDueDate: string;
    lastPaymentAmount: number;
    frequency: string;
    remainingTerm: number;
  }[];
};

export type LoanDetailsType = {
  paymentAmount: number;
  totalAmountToCurrent: number;
  lastPaymentDate: string;
  loanAmountToCurrent: number;
  principalRemaining: number;
  loanOldestDueDate: string;
  firstPaymentScheduled: boolean;
  interestPaid: number;
  nextDueDate: string;
  nextDebitDate: string;
  lastDueDate: string;
  overDueDays: number;
  interestRemaining: number;
  loanPayOffAmountAsOfToday: number;
  principalPaid: number;
  disbursalAmount: number;
  lastPaymentAmount: number;
  subSource: string;
  isDebit: boolean;
  isAch: boolean;
  isAutopay: boolean;
  loanId?: string;
  apr: number;
  term: number;
  frequency: string;
  remainingTerm: number;
  depositAccountNumber?: string;
  depositBankName?: string;
  withdrawalAccountNumber?: string;
  withdrawalBankName?: string;
  debitCardNumber?: string;
  expiryDate?: string;
  yearlyPaymentAmount?: number;
};

export type PaymentFromApi = {
  transactionId: string;
  transactionNumber: string;
  date: string;
  interest: number;
  total: number;
  principal: number;
  type: string;
  method: string;
};

export type PaymentHistoryResponse = {
  details: {
    transactionId: string;
    transactionNumber: string;
    date: string;
    interest: number;
    total: number;
    principal: number;
    type: string;
    method: string;
    paymentMethodNumber: any;
  }[];
};

export { testData };
