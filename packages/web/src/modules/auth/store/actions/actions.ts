import { createAction } from 'typesafe-actions';
import types from './types';

export const logout = createAction(types.USER.LOGOUT);
export const setIsAuthLoading = createAction(types.AUTH.LOADING, action => (bool: boolean) =>
  action(bool),
);
