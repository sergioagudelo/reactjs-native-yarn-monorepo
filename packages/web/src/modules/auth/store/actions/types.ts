import { NAME } from '@customer-portal/common/src/modules/auth/constants';
import shared from '../../../shared';

export default shared.helpers.createActionTypes(NAME, ['USER.LOGOUT', 'AUTH.LOADING']);
