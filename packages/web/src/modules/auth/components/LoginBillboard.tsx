import React from 'react';

// Images
import imgVirtualcard from "images/billboard/lp-virtualcard.jpg";
import imgApp from "images/billboard/lp-app.jpg";
import imgAutopay from "images/billboard/lp-autopay.jpg";

const LoginBillboard = () => {
  return (
    <div className="lcp-form-box lcp-form-right">
      <div className="uk-card lcp-billboard" uk-height-viewport="true">
        <div className="uk-position-relative uk-visible-toggle uk-light uk-height-1-1" data-tabindex="-1" uk-slideshow="autoplay: true; autoplay-interval: 6000; animation: scale">
          <ul className="uk-slideshow-items uk-height-1-1">
            <li>
              <img src={imgVirtualcard} alt="virtualcard" uk-cover="true" />
            </li>
            <li>
              <img src={imgApp} alt="vpp" uk-cover="true" />
            </li>
            <li>
              <img src={imgAutopay} alt="vutopay" uk-cover="true" />
            </li>
          </ul>
          <div className="uk-position-bottom-center uk-position-small">
            <ul className="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
          </div>
        </div >
      </div>
    </div>
  );
};

export default LoginBillboard;