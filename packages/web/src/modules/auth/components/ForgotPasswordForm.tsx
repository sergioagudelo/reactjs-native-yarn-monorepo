import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormikProps, Form, Field, ErrorMessage } from 'formik';

// Types
import { ForgotPasswordFormValues } from '../utils';

// REV: Movil
// type ForgotPasswordFormComponentProps = ThemedComponentProps &
//   NavigationStackScreenProps &
//   FormikProps<ForgotPasswordFormValues>;
type ForgotPasswordFormComponentProps = FormikProps<ForgotPasswordFormValues>;

const ForgotPasswordForm = ({
  values,
  touched,
  errors,
  status,
  setStatus,
  handleBlur,
  handleChange,
  handleSubmit,
  isValid,
  isSubmitting,
  // theme, REV: Movil
  // themedStyle,
  // navigation,
}: ForgotPasswordFormComponentProps) => {

  const history = useHistory();

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalWasClosed, setModalWasClosed] = useState(false);

  const onDismiss = () => {
    setIsModalVisible(false);
    setStatus({});
    if (status.success) {
      history.replace('/login');
    }
  };

  useEffect(() => {
    if (!isModalVisible && status && (status.success || status.error)) {
      setIsModalVisible(true);
    }
  }, [isModalVisible, status]);

  return (
    <Form className="uk-form-stacked lcp-form uk-margin-medium">
      <div className="uk-margin">
        <label className="uk-form-label">Enter account email address</label>
        <div className="uk-form-controls">
          <Field className="uk-input" name="email" type="email" placeholder="myemail@email.com" required />
        </div>
        <span className="validation-error uk-text-danger uk-hidden"><img className="uk-icon" src="../images/icons/ico-error.svg" alt="" /> Information required</span>
      </div>
      <div className="uk-margin uk-hidden">
        <div className="uk-alert-warning" uk-alert="true">
            <button className="uk-alert-close" uk-close="true"></button>
            <p>email does not exist / is incorrect. Please try again.
            </p>
        </div>
      </div>
      <div className="uk-text-center uk-margin-medium">
        <button type="submit" name="button" className="uk-button uk-button-primary">Submit</button>
      </div>
      <div className="uk-text-center uk-margin">
        <Link to="/auth" className="uk-button uk-button-link">Back to sign-in</Link>
      </div>

    </Form>
  );
};

export default ForgotPasswordForm;

