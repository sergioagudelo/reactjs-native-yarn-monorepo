import React from 'react';
import { FormikProps, Form, Field, ErrorMessage } from 'formik';
import { useRouteMatch, Link } from 'react-router-dom';

// Types
import { RootState } from 'typesafe-actions';
import { connect } from 'react-redux';
import { LoginFormValues } from '../utils';

import * as authStore from '../store';

// Modules
// import shared from '../../shared';

// Constants
import { forms } from '@customer-portal/common/src/modules/auth/constants';

// Config
import config from 'config';

// Images
import imgError from 'images/icons/ico-error.svg';
import coin from 'images/icons/coin.svg';
import coinHelpSaver from 'images/icons/coin-helpsaver.svg';

// // Components
// const { PasswordInput } = shared.components;

const mapStateToProps = (state: RootState) => ({
  isAuthLoading: authStore.selectors.getIsAuthLoading(state),
});
// REV: mobile,
// type LoginFormComponentProps = ThemedComponentProps &
type LoginFormComponentProps = FormikProps<LoginFormValues> &
  ReturnType<typeof mapStateToProps>;

const LoginForm = ({
  values,
  touched,
  errors,
  handleBlur,
  handleChange,
  isValid,
  isSubmitting,
  handleSubmit,
  isAuthLoading,
}: LoginFormComponentProps) => {
  // REV: mobile,
  // const emailInputRef = React.useRef<HTMLInputElement>(null);
  // const passwordInputRef = React.useRef<HTMLInputElement>(null);

  let { url } = useRouteMatch();

  return (
    <React.Fragment>
      <Form className="uk-form-stacked lcp-form uk-margin-medium">
        <div className="uk-margin uk-text-center">
          <label className="uk-form-label">Email Address</label>
          <div className="uk-form-controls">
            <Field
              className="uk-input"
              type="email"
              name="email"
              disabled={isAuthLoading}
              // ref={emailInputRef}
              // REV: mobile,
              // status={shared.helpers.getInputStatus<LoginFormValues>('email', { touched, errors })}
              // keyboardType="email-address"
              // textContentType="emailAddress"
              // autoCompleteType="email"
              // returnKeyType="next"
              // onSubmitEditing={() => {
              //   if (passwordInputRef && passwordInputRef.current) {
              //     passwordInputRef.current.focus();
              //   }
              // }}
              // value={values.email}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={forms.login.placeholders.email}
              autoCapitalize="none"
            />
          </div>
          <ErrorMessage name="email">
            {message =>
              <div className="error">
                <span className="validation-error uk-text-danger">
                  <img className="uk-icon" src={imgError} alt="Error" />{message}
                </span>
              </div>
            }
          </ErrorMessage>
        </div>
        <div className="uk-margin uk-text-center">
          <label className="uk-form-label">Password</label>
          <div className="uk-form-controls">
            <Field
              className="uk-input"
              type="password"
              name="password"
              disabled={isAuthLoading}
              // ref={passwordInputRef}
              // REV: mobile,
              // status={shared.helpers.getInputStatus<LoginFormValues>('password', { touched, errors })}
              // label={forms.login.labels.password}
              // caption={touched.password && errors.password ? errors.password : ''}
              // labelStyle={themedStyle.label}
              // textContentType="password"
              // value={values.password}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={forms.login.labels.password}
            />
          </div>
          <ErrorMessage name="password">
            {message =>
              <div className="error">
                <span className="validation-error uk-text-danger"><img className="uk-icon"
                  src={imgError} alt="Error" />{message}
                </span>
              </div>}
          </ErrorMessage>
        </div>
        {errors.submit && (
          <div className="uk-margin">
            <div className="uk-alert-warning" uk-alert="true">
              <button className="uk-alert-close" uk-close="true"></button>
              <p>
                Email or password are incorrect.
                Please try again, or recover your account password.
              </p>
            </div>
          </div>
        )}
        <div className="uk-text-center uk-margin-top">
          {!isSubmitting ? (
            <button
              className="uk-button uk-button-primary"
              type="submit"
              disabled={!isValid || isSubmitting || isAuthLoading}
            >
              Enter
            </button>
          ) : (
              <button
                className="uk-button uk-button-primary loading"
                type="submit"
                disabled={true}
              >
                Enter
              </button>
            )}
        </div>
        <div className="uk-text-center uk-margin">
          <Link to={`${url}/forgot-password`}
            className={isAuthLoading ? 'disabled-link uk-button uk-button-link' : 'uk-button uk-button-link'
            }>
            Forgot Password?
          </Link>
        </div>
        <h2 className="uk-text-xlarge uk-text-light uk-text-lpblue uk-text-center uk-margin-medium-top">Not a customer yet?</h2>
        <div className="lcp-buttons uk-margin-medium-bottom">
          <a
            href={config.applyUrl}
            target="_blank"
            rel="noopener noreferrer"
            className={isAuthLoading ?
              'disabled-link uk-link-reset lcp-minibutton uk-flex uk-flex-middle uk-flex-left' :
              ' uk-link-reset lcp-minibutton uk-flex uk-flex-middle uk-flex-left'}
          >
            <img src={coin} alt="" />
            <div>
              <strong className="uk-text-bluedark">Apply for a loan</strong>
              <span className="uk-text-meta">Financing options up to $25,000</span>
            </div>
          </a>
          <a
            href={config.contactUrl}
            target="_blank"
            rel="noopener noreferrer"
            className={isAuthLoading ?
              'disabled-link uk-link-reset lcp-minibutton uk-flex uk-flex-middle uk-flex-left' :
              ' uk-link-reset lcp-minibutton uk-flex uk-flex-middle uk-flex-left'}
          >
            <img src={coinHelpSaver} alt="coinHelpSaver" />
            <div>
              <strong className="uk-text-bluedark">Contact Support</strong>
              <span className="uk-text-meta">LendingPoint Support channels</span>
            </div>
          </a>
        </div>
      </Form>
    </React.Fragment >
  );
};

export default connect(mapStateToProps)(LoginForm);