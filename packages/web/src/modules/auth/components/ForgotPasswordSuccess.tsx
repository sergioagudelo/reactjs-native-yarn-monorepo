import React from 'react'
import imgEmailSent from 'images/lp-email-sent.svg'
import imgIconGreen from 'images/icons/ico-ok-green.svg'

const ForgotPasswordSuccess = () => {
  return (
    <form className="uk-form-stacked lcp-form uk-margin-medium">
      <div className="uk-margin uk-text-center">
        <img src={imgEmailSent} alt="" />
        <p className="uk-text-xlarge uk-text-light uk-text-lpblue">We've sent you a password reset link to</p>
        <p className="uk-text-bluedark uk-text-medium">****ail@***ail.com</p>
        <img src={imgIconGreen} alt="" />
      </div>
    </form>
  )
}

export default ForgotPasswordSuccess;