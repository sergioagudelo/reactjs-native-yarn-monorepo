import React from 'react';
import { NavLink } from 'react-router-dom';

// Images
import imgLendingpoint from 'images/lendingpoint.svg';

const LoginHeader = ({subtitle={}}) => {
  return (
    <div className="lcp-head">
      <nav className="uk-navbar-container uk-navbar-transparent" uk-navbar="true">
        <div className="uk-navbar-center uk-width-expand">
          <ul className="uk-width-expand uk-navbar-nav uk-flex uk-flex-middle uk-flex-center">
            <li>
              <NavLink to="/" className="uk-navbar-item uk-logo">
                <img src={imgLendingpoint} alt="" />
              </NavLink>
            </li>
            <li hidden>
              <a className="uk-navbar-item uk-logo uk-text-left" href="/">
                <img src="images/lp-square.svg" alt="" />
              </a>
            </li>
            <li hidden>
              <a className="uk-navbar-item uk-logo uk-text-right" href="/">
                <img src="images/ebay.png" alt="" />
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <div className="lcp-form-head uk-text-center">
        <hr />
        <div className="uk-text-meta">{subtitle}</div>
      </div>
    </div>
  );
};

export default LoginHeader;
