// import { Keyboard } from 'react-native';
import { compose, Dispatch, bindActionCreators } from 'redux';
import { withFormik } from 'formik';
import { connect } from 'react-redux';
// import { withNavigation } from 'react-navigation';
// import { NavigationStackProp } from 'react-navigation-stack';
// import { auth as loginService, keychain } from '../services';
import { auth as loginService } from '../services';
import { withRouter } from 'react-router-dom';


// Types
import { LoginFormValues } from '../utils';

// Constants
import { forms } from '@customer-portal/common/src/modules/auth/constants';

// Store
import * as profileStore from '../../profile/store';
import * as authStore from '../store';

// Component
import LoginForm from '../components/LoginForm';
// import { logEventToAnalytics, LOGGING_SUCCESSFUL_EVENT } from '../../../utils';

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getUserInfo: profileStore.actions.thunks.userInfo,
      setIsAuthLoading: authStore.actions.actions.setIsAuthLoading,
    },
    dispatch,
  );

type LoginFormProps = {
  // navigation: NavigationStackProp;
  history: any,
  initialValues?: LoginFormValues;
} & ReturnType<typeof mapDispatchToProps>;

export default compose(
  // withNavigation,
  withRouter,
  connect(
    null,
    mapDispatchToProps,
  ),
  withFormik<LoginFormProps, LoginFormValues>({
    mapPropsToValues: () => ({
      email: forms.login.initialValues.email,
      password: forms.login.initialValues.password,
    }),

    isInitialValid: props => forms.login.schema.isValidSync(props.initialValues),
    validationSchema: forms.login.schema,
    handleSubmit: async (values, { setSubmitting, setFieldError, props }) => {
      const { email, password } = values;

      props.setIsAuthLoading(true);
      // Keyboard.dismiss();

      const { success, details } = await loginService.login({
        email,
        password,
        username: email.trim(),
      });

      if (success) {
        // logEventToAnalytics(LOGGING_SUCCESSFUL_EVENT);
        // save on keychain

        // await keychain.setGenericCredentials({ email, password });
        // keep loader until profile is fetched
        await props.getUserInfo();
        setSubmitting(false);
        props.setIsAuthLoading(false);

        return props.history.push('/loans')

      }
      // failed, reset keychain
      // await keychain.resetCredentials();
      setSubmitting(false);
      props.setIsAuthLoading(false);
      // ? result result.error
      // ? Show the errors for the requests that failed

      setFieldError('submit', details ?? 'Something went wrong. Please try again');
    },
  }),
  // TODO FIX PROPS TYPING ISSUE
)(LoginForm) as React.ComponentType;
