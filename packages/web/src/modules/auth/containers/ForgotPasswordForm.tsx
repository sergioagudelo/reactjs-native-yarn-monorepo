// import { Keyboard } from 'react-native';
import { compose } from 'redux';
import { withFormik } from 'formik';

// Types
import { ForgotPasswordFormValues } from '../utils';

// Constants
import { forms } from '@customer-portal/common/src/modules/auth/constants';

// Component
import ForgotPasswordForm from '../components/ForgotPasswordForm';

// services
import * as loginService from '../services/auth';

type ForgotPasswordFormProps = {
  initialValues?: any;
  setIsEmailSentState: any;
};

export default compose(
  withFormik<ForgotPasswordFormProps, ForgotPasswordFormValues>({
    mapPropsToValues: () => ({
      email: forms.forgotPassword.initialValues.email,
    }),
    isInitialValid: props => forms.forgotPassword.schema.isValidSync(props.initialValues),
    validationSchema: forms.forgotPassword.schema,
    handleSubmit: (values, { setSubmitting, setStatus, props }) => {
      console.log('hola mundo: ', props);
      setStatus({});
      const sendResetPasswordMail = async (email: string) => {
        // Keyboard.dismiss();

        setSubmitting(true);
        const result = await loginService.resetPassword(email);
        setSubmitting(false);

        if (result.success) {
          // props.history.replace('/auth/email-send');
          setStatus({
            success: true,
            successMsg: "We've sent a password reset link to",
          });
          props.setIsEmailSentState(true);
        } else {
          // ? Show the errors for the requests that failed
          setStatus({
            success: false,

            error: result.details || 'An error ocurred.',
          });
        }
      };
      sendResetPasswordMail(values.email);
    },
  }),
)(ForgotPasswordForm) as new () => React.Component<any, any>;