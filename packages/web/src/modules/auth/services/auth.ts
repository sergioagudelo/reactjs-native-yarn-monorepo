import { lendingpoint } from '../../../services';
import { ApiResponse } from '@customer-portal/common/src/services/utils';
import { LoginPayload, LoginResponse } from '@customer-portal/common/src/modules/auth/utils';
// import shared from '../../shared';
import config from '@customer-portal/common/src/config';
//import { refreshToken } from '../../../services/modules/lendingpoint/api/consumer';
import type { AxiosError } from 'axios';

const {
  api: { manager },
  constants: { paths },
  helpers: { setAuthTokens, handleSubModuleError },
} = lendingpoint;

// const { PushNotifications, DeviceInfo } = shared.services;
const {
  api: { host },
} = config;
export const login = async (
  payload: LoginPayload
): Promise<ApiResponse<LoginResponse>> => {
  try {
    let headers = null;
    headers = {
      // 'lp-device-token': await PushNotifications.registerForPush(),
      // 'lp-device-info': await DeviceInfo.getDeviceInfoForPush(),
    };
    const response = await manager.post(
      paths.login,
      {
        username: payload.username,
        password: payload.password,
        grantType: 'password',
      },
      headers ? { headers } : undefined
    );
    const scope = response.data.scope;
    const isFunded = !!scope?.isFunded;

    isFunded && setAuthTokens(response.data);
    //await refreshToken();
    return { success: isFunded, response: response.data };
  } catch (err) {
    return Object.assign(handleSubModuleError(err), { response: (err as AxiosError).response?.data });
  }
};

export const resetPassword = async (
  email: string
): Promise<ApiResponse<any>> => {
  try {
    const response = await manager.post(paths.resetPassword, {
      email,
      redirectUrl: config.redirectUrl,
    });
    return { success: true, response };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
