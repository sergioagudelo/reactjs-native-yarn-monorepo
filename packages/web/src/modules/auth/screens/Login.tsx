import React from 'react';

// Components
import { LoginForm } from '../containers';
import { LoginHeader, LoginBillboard } from '../components'

// Store
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import * as authStore from '../store';

// REV: mobile,
// Components
// const { CardButton } = shared.components;

const mapStateToProps = (state: RootState) => ({
  isAuthLoading: authStore.selectors.getIsAuthLoading(state),
  userInfo: authStore.selectors.getUserInfo(state),
  // REV: mobile,
  // currentRouteName: authStore.selectors.getCurrentRouteName(state),
  // previousRouteName: authStore.selectors.getPreviousRouteName(state)
});


type LoginProps =
  // REV: mobile,
  // ThemedComponentProps &
  // NavigationStackScreenProps &
  ReturnType<typeof mapStateToProps>;

const LoginComponent = ({
  isAuthLoading,
  userInfo,
}: LoginProps) => {

  return (
    <div className="lcp-form-container" uk-height-viewport="expand: true">
      <div className="lcp-login uk-grid-collapse uk-child-width-1-2@s uk-grid-match uk-flex-middle" uk-grid="true">
        <div className="lcp-form-box lcp-form-left">
          <div className="uk-card">
            <LoginHeader subtitle={"Enter your account"} />
            <LoginForm />
          </div>
        </div>
        <LoginBillboard />
      </div>
    </div>
  );
};


export default connect(mapStateToProps)(LoginComponent);