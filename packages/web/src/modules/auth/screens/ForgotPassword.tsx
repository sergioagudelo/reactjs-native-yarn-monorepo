import React from 'react';
import LoginBillboard from '../components/LoginBillboard';
import LoginHeader from '../components/LoginHeader';
import  { ForgotPasswordForm }  from '../containers';
import { ForgotPasswordSuccess } from '../components';
import { useState } from 'react';

const ForgotPassword = () => {
  
  const [isEmailSentState, setIsEmailSentState] = useState(false);

  const propsFPF = {
    setIsEmailSentState
  }

  return (
    <div className="lcp-form-container" uk-height-viewport="expand: true">
      <div className="lcp-login uk-grid-collapse uk-child-width-1-2@s uk-grid-match uk-flex-middle" uk-grid="true">
        <div className="lcp-form-box lcp-form-left">
          <div className="uk-card">
            <LoginHeader subtitle={"Reset your password"} />
            {
              isEmailSentState? <ForgotPasswordSuccess /> : <ForgotPasswordForm setIsEmailSentState={setIsEmailSentState} />
            }
            {/* <ForgotPasswordForm />
            <ForgotPasswordSuccess /> */}
          </div>
        </div>
        <LoginBillboard />
      </div>
    </div>
  );
};

export default ForgotPassword;