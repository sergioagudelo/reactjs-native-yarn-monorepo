import React from 'react'
import LoginHeader from '../components/LoginHeader';
import LoginBillboard from '../components/LoginBillboard';
import { ForgotPasswordSuccess as FPS } from '../components/';

const ForgotPasswordSuccess = () => {
    return (
        <div className="lcp-form-container" uk-height-viewport="expand: true">
      <div className="lcp-login uk-grid-collapse uk-child-width-1-2@s uk-grid-match uk-flex-middle" uk-grid="true">
        <div className="lcp-form-box lcp-form-left">
          <div className="uk-card">
            <LoginHeader subtitle={"Reset your password"} />
            <FPS />
          </div>
        </div>
        <LoginBillboard />
      </div>
    </div>
    )
}

export default ForgotPasswordSuccess;
