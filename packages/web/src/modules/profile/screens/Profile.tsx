import React from 'react';
// import { SafeAreaView, ScrollView, StyleSheet } from 'react-native';
// import { ThemedComponentProps, Layout } from 'react-native-ui-kitten';
// import { NavigationTabScreenProps } from 'react-navigation-tabs';

// store
import { useDispatch } from 'react-redux';
import { actions as authActions } from '../../auth/store/actions';
// Types

import { ProfileSettingProps } from '../utils';

// import { setTestID } from '../../shared/helpers';
// import { HeaderView, VerticalCardButton, AppVersionLabel } from '../../shared/components';

// Constants
// import { profileButtons } from '../constants';

// Components
import { Profile } from '../containers/';
// import {
//   logEventToAnalytics,
//   USER_SIGNED_OUT_EVENT,
//   USER_PRESSED_PROFILE_OPTION_EVENT,
// } from '../../../utils/analytics';

// type ProfileProps = NavigationTabScreenProps & ThemedComponentProps;

import '../styles/my-account.scss'
import '../../../assets/styles/cta-footer.scss'

import icoProfile from '../../../images/icons/ico-profile.svg'
import icoSecurity from '../../../images/icons/ico-security.svg'
import icoDocuments from '../../../images/icons/ico-documents.svg'
import icoMethods from '../../../images/icons/ico-methods.svg'
import icoHelpCenter from '../../../images/icons/ico-helpcenter.svg'
import icoExit from '../../../images/icons/ico-exit.svg'
import icoOkWhite from '../../../images/icons/ico-ok-white.svg'
import lpVirtualCard from '../../../images/lp-virtualcard.png'

const ProfileScreen = () => {
  const dispatch = useDispatch();

  return (
    <>
            <div className="lcp-container uk-background-gray" uk-height-viewport="expand: true">
          <form className="uk-form-stacked">
            <div className="uk-container uk-container-xsmall">
              <div className="lcp-account uk-grid-collapse uk-grid-match uk-flex" uk-grid="true">
                <Profile />
                <div className="uk-width-expand">
                  <div className="uk-card lcp-profile-buttons uk-flex uk-flex-top uk-grid-collapse uk-child-width-1-3" uk-grid="true" uk-height-match="true">
                    <a className="uk-button" href="#"><img src={icoProfile} alt="" /><span>Profile</span></a>
                    <a className="uk-button" href="#"><img src={icoSecurity} alt="" /><span>Security</span></a>
                    <a className="uk-button" href="#"><img src={icoDocuments} alt="" /><span>Documents</span></a>
                    <a className="uk-button" href="#"><img src={icoMethods} alt="" /><span>Payment Methods</span></a>
                    <a className="uk-button" href="#"><img src={icoHelpCenter} alt="" /><span>Help Center</span></a>
                    <a className="uk-button" href="#"><img src={icoExit} alt="" /><span>Sign Out</span></a>
                  </div>
                </div>
              </div>
              <div className="uk-text-center uk-margin-large lcp-notification">
                <h2 className="uk-text-lead uk-text-bluedark uk-text-center uk-margin-small">Recent Notifications</h2>
                <a className="uk-label uk-label-info uk-label-pill">
                  You have 2 unread notifications! <img className="uk-icon" src={icoOkWhite} alt="" />
                </a>
              </div>

            </div>

          </form>
        </div>


      <div className="lcp-cta-footer uk-background-gray-bottom uk-child-width-auto uk-grid-match uk-flex-center uk-grid-collapse" uk-grid="true">
        <div className="lcp-cta lcp-virtualcard">
          <a className="uk-card uk-card-small">
            <img src={lpVirtualCard} alt="" />
          </a>
        </div>
      </div>
    </>
    );
};


export default ProfileScreen;
