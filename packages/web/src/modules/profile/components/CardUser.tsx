import React, { ReactElement } from 'react';
import { useSelector } from 'react-redux';


import { getUserInfo } from '../store/selectors';

// Modules
import shared from '../../shared';
// import { Styles } from '../helpers';

/**
 * component type
 */
// type CardUserProps = ThemedComponentProps;

/**
 * render userInfo with edit button
 */

import profile from '../../../images/icons/profile.svg'
import icoPhoneBlue from '../../../images/icons/ico-phone-blue.svg'
import icoEmailBlue from '../../../images/icons/ico-email-blue.svg'

const CardUserElement = (
//   {
//   themedStyle,
// }: CardUserProps
): ReactElement | null => {
  const userInfo = useSelector(getUserInfo);

  if (!userInfo) {
    return null;
  }

  return (
    <div className="uk-width-2-5@s">
      <div className="uk-card uk-card-default uk-card-small uk-card-body lcp-profile uk-flex uk-flex-top">
        <img className="lcp-profile-left" src={profile} alt="" />
        <div className="uk-width-expand lcp-profile-right">
          <div className="uk-text-right uk-margin">
            <a href="#" className="uk-button uk-button-link">Edit</a>
          </div>
          <div className="lcp-profile-info">
            <p className="uk-text-large uk-text-normal uk-text-bluedark uk-margin">John Smith</p>
            <p className="uk-text-content uk-text-graydarkx uk-margin-small"><img src={icoPhoneBlue} alt="" /> (409) 974-6766</p>
            <p className="uk-text-content uk-text-graydarkx uk-margin-small uk-margin-remove-bottom"><img src={icoEmailBlue} alt="" /> ••••h34@gmail.com</p>
          </div>
        </div>
      </div>
    </div>
  );
};

const CardUser = CardUserElement

export default CardUser;
