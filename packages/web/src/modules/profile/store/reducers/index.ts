import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import type { User } from '../../utils';

import { actions } from '../actions';
// REV: changed null for undefined
// const userInfo = createReducer(null as User | null).handleAction(
const userInfo = createReducer(null as User | undefined | null).handleAction(
  actions.UserInfo.success,
  (state, action) => action.payload,
);

const userReducer = combineReducers({
  userInfo,
});

export default userReducer;
export type UserState = ReturnType<typeof userReducer>;
