import { createAsyncAction } from 'typesafe-actions';

// Types
import { User } from '../../utils';

import types from './types';

export const UserInfo = createAsyncAction(
  types.USER.ATTEMPT,
  types.USER.SUCCESS,
  types.USER.FAILURE,
)<undefined, User | undefined | null, Error | undefined>();
