/* eslint-disable global-require */
import React, { Fragment, useRef, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
// import { View, Image, Platform, Alert } from 'react-native';
// import {
//   withStyles,
//   Text,
//   ThemedComponentProps,
//   ListItem,
//   Icon,
//   Button,
//   Layout,
// } from 'react-native-ui-kitten';
// import ViewShot from 'react-native-view-shot';
// import RNFetchBlob from 'rn-fetch-blob';

import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

import * as store from '../../profile/store';

import * as api from '../services/payments';

// Types
import { PaymentType } from '../utils';

// Modules
import shared from '../../shared';
import { PaymentMethodTypeKeys } from '../constants';


import icoOkWhite from '../../../images/icons/ico-ok-white.svg'
import icoOkGray from '../../../images/icons/ico-ok-gray.svg'
import icoEmailGray from '../../../images/icons/ico-email-gray.svg'
import icoPrintGray from '../../../images/icons/ico-print-gray.svg'
import icoHomeGray from'../../../images/icons/ico-home-gray.svg'
import lpVC from'../../../images/lp-virtualcard.png'

// import { Modal } from '../../shared/components';

// Components
// const { Dashed, Loading } = shared.components;

const mapStateToProps = (state: RootState) => ({
  userInfo: store.selectors.getUserInfo(state),
});

type PaymentReceiptComponentProps = {
  payment: PaymentType;
} &
// & ThemedComponentProps &
  ReturnType<typeof mapStateToProps>;

const PaymentReceiptComponent = ({
  payment,
  userInfo,
}: PaymentReceiptComponentProps) => {
  console.log('info payments: ', payment);
  const paymentMethodUsedIsAch = payment.mode === PaymentMethodTypeKeys.BANK_ACCOUNT || false;
  payment.date = payment.date // || ('' as Date);
  // const receiptShot = useRef<ViewShot>(null);
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [modalData, setModalData] = useState();

  const sendReceipt = async () => {
    setIsLoading(true);
    const result = await api.sendReceipt(payment);
    console.log('result: ', result);
    setIsLoading(false);
    if (result.success) {
    //   setModalData({
    //     title: 'Payment Receipt succesfully sent!',
    //     options: [`An email was sent to ${payment.email}`],
    //     buttonTitle: 'Ok',
    //     visible: true,
    //   });
      return;
    }

    // setModalData({
    //   title: "Can't send the email",
    //   options: [result.error],
    //   buttonTitle: 'Close',
    //   visible: true,
    // });
  };

//   const printReceipt = async () => {
//     if (receiptShot.current && receiptShot.current.capture) {
//       try {
//         const capture = RNFetchBlob.base64.encode(
//           RNFetchBlob.base64.decode(await receiptShot.current.capture()),
//         );
//         const captureName = `receipts/${`${payment.transactionId}_`??''}${
//           payment.date ? payment.date.toString() : Date.now()
//         }.jpg`;
//         const captureMime = 'image/jpeg';
//         const captureUri = await shared.helpers.saveFile(captureName, capture);

//         if (Platform.OS === 'android') {
//           RNFetchBlob.android.actionViewIntent(captureUri, captureMime);
//         } else {
//           RNFetchBlob.ios.previewDocument(captureUri);
//         }
//       } catch (e) {
//         Alert.alert('Oops', `There was an error saving the receipt:\n${e.message}`);
//       }
//     }
//   };

  return (
    <Fragment>
    <div className="lcp-container uk-background-default" uk-height-viewport="expand: true">
            <div className="uk-background-success uk-text-center uk-margin-medium-bottom">
              <div className="uk-alert-transparent" uk-alert="true">
                  <p className="uk-text-white uk-text-intro"><img className="uk-margin-small-right" src={icoOkWhite} alt="" /> Your payment has been received. Thank you, John</p>
              </div>
            </div>
            <h2 className="uk-text-lead uk-text-bluedark uk-text-center uk-margin-small">Payment Receipt</h2>
          <form className="uk-form-stacked">
            <div className="uk-container uk-container-xsmall lcp-receipt uk-margin-top">
              <div className="uk-card uk-card-body">
                <div className="uk-flex uk-flex-middle uk-flex-between lcp-payment-info">
                  <div>
                    <img className="lcp-icon-ok" src={icoOkGray} alt="" />
                    <small className="uk-text-content uk-text-graydark uk-text-uppercase">Payment amount due</small>
                  </div>
                  <span className="uk-text-medium uk-text-bluedark">{shared.helpers.format.formatMoney(payment.amount, {
                                                                      minimumFractionDigits: 2,
                                                                      maximumFractionDigits: 2,
                                                                    })}
                </span>
                </div>
                <div className="uk-flex uk-flex-middle uk-flex-between lcp-payment-info">
                  <div>
                    <img className="lcp-icon-ok" src={icoOkGray} alt="" />
                    <small className="uk-text-content uk-text-graydark uk-text-uppercase">Payment Method</small>
                  </div>
                  <span className="uk-text-medium uk-text-bluedark">Debit Card ••••• 5675</span> {/* payment.paymentMethod */}
                </div>
                <div className="uk-flex uk-flex-middle uk-flex-between lcp-payment-info">
                  <div>
                    <img className="lcp-icon-ok" src={icoOkGray} alt="" />
                    <small className="uk-text-content uk-text-graydark uk-text-uppercase">Payment Date</small>
                  </div>
                  <span className="uk-text-medium uk-text-bluedark">Wednesday, September 22, 2020</span> {/* shared.helpers.format.formatDate(
                payment.date.toString(),
                'dddd MMM Do, YYYY',
              ) */}
                </div>
                <div className="uk-flex uk-flex-middle uk-flex-between lcp-payment-info">
                  <div>
                    <img className="lcp-icon-ok" src={icoOkGray} alt="" />
                    <small className="uk-text-content uk-text-graydark uk-text-uppercase">Total Paid</small>
                  </div>
                  <span className="uk-text-medium uk-text-bluedark">{shared.helpers.format.formatMoney(payment.total || 0, {
                                                                      minimumFractionDigits: 2,
                                                                      maximumFractionDigits: 2,
                                                                    })}
                </span>
                </div>
              </div>
              <div className="uk-text-center uk-margin-medium-top" uk-margin="true">
                <button className="uk-button uk-button-secondary uk-margin-small-right uk-button-small" type="button" uk-toggle="target: #emailModal" >
                    <span><img src={icoEmailGray} alt="" /></span> Send to email
                </button>
                <button className="uk-button uk-button-secondary uk-button-small" type="button" onClick={()=>window.print()} >
                    <span><img src={icoPrintGray} alt="" /></span> Print Receipt
                </button>
              </div>
              <div className="uk-text-center uk-margin-medium">
                <a onClick={()=>history.replace('/loans')} className="uk-button uk-button-link"><span className="uk-icon"><img src={icoHomeGray} alt="" /></span> Back to Loans</a>
              </div>
            </div>
          </form>

          </div>

          <div id="emailModal" uk-modal="true">
              <div className="uk-modal-dialog uk-modal-body">
                  <h2 className="uk-modal-title">Receipt sent</h2>
                  <p className="uk-text-right">
                    <button className="uk-button uk-button-primary uk-modal-close" type="button">OK</button>
                </p>
              </div>
          </div>

          <div className="lcp-cta-footer uk-background-gray-bottom uk-child-width-auto uk-grid-match uk-flex-center uk-grid-collapse" uk-grid="true">
          <div className="lcp-cta lcp-virtualcard">
            <a className="uk-card uk-card-small">
              <img src={lpVC} alt="" />
            </a>
          </div>
          </div>
      </Fragment>
  );
};

export default connect(mapStateToProps)(PaymentReceiptComponent);
