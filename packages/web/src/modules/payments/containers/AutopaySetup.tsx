import React, { useState } from 'react';
import { withRouter, RouteComponentProps, useHistory } from 'react-router-dom';

// Store
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import { Dispatch, bindActionCreators } from 'redux';
import * as store from '../store';
// FIXME loan details circular dependencies
import * as loanStore from '../../loans/store';

// Modules
import profile from '../../profile';
import LoanBalanceHeader from '../../loans/components/LoanBalanceHeader';
import { AutopaySetup as AutopaySetupComponent } from '../components';
import { PaymentMethodType } from '@customer-portal/common/src/modules/payments/utils';
import { ApiResponse } from '@customer-portal/common/src/services/utils';
import * as loanService from '../../loans/services/loans';

const { store: profileStore } = profile;

type LoanDetailsMapStateProps = {
  loanId: string;
};

const mapStateToProps = (state: RootState, { loanId }: LoanDetailsMapStateProps) => ({
  userInfo: profileStore.selectors.getUserInfo(state),
  paymentMethods: store.selectors.getPaymentMethods(state),
  defaultPaymentMethod: store.selectors.getDefaultPaymentMethod(state),
  loan: loanStore.selectors.getLoanById(state, loanId),
  loanDetails: loanStore.selectors.getLoanDetails(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getLoanDetails: loanStore.actions.thunks.getDetails,
    },
    dispatch,
  );

type MakePaymentProps = LoanDetailsMapStateProps &
  RouteComponentProps &
  ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const AutopaySetup = ({
  paymentMethods,
  defaultPaymentMethod,
  loan,
  userInfo,
  getLoanDetails,
}: MakePaymentProps) => {
  let history = useHistory();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState<string | undefined>();
  const handleNewPaymentMethod = () => {
    history.push('PaymentMethods');
  };

  const handleSetAsAutopay = async (method: PaymentMethodType): Promise<ApiResponse> => {
    const loanId = loan ? loan.loanId : ' ';
    setIsSubmitting(true);
    setError(undefined);
    const result = await loanService.setAutoPayMethod(loanId, method);
    if (result.success) {
      await getLoanDetails(loanId);
      setIsSubmitting(false);
      history.goBack();
      return result;
    }
    setIsSubmitting(false);
    setError(result.details);
    return result;
  };
  return (
    <React.Fragment>
      <div className="lcp-container uk-background-blue">
        {loan ? <LoanBalanceHeader lai={loan.lai} balance={loan.balance} /> : null}
        <br />
        <br />
        <br />
      </div>
      <AutopaySetupComponent
        userFullName={userInfo ? `${userInfo.firstName} ${userInfo.lastName}` : ''}
        defaultPaymentMethod={defaultPaymentMethod}
        paymentMethods={paymentMethods}
        isLoading={isSubmitting}
        errorMsg={error}
        setAsAutopay={handleSetAsAutopay}
        onNewPaymentMethod={handleNewPaymentMethod}
      />
    </React.Fragment>
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(AutopaySetup));
