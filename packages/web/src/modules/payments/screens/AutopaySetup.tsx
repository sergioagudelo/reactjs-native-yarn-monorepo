/* eslint-disable global-require */
import React from 'react';
import { useLocation } from 'react-router-dom';

// Modules
import shared from '../../shared';
import { AutopaySetup } from '../containers';

type LocationParams = {
  loanId?: string
}

const AutopaySetupScreen = () => {
  const loanId = useLocation<LocationParams>().state?.loanId || 'empty';

  return (
    <React.Fragment>
      <AutopaySetup loanId={loanId} />
    </React.Fragment>
  );
};

export default AutopaySetupScreen;
