import React from 'react';
// Rev: Movil
// import { SafeAreaView, ScrollView, View } from 'react-native';
// import { withStyles, ThemedComponentProps, Button, Layout } from 'react-native-ui-kitten';
// import { NavigationStackScreenProps } from 'react-navigation-stack';
import { withRouter } from 'react-router-dom';

// Types
import { PaymentType, PaymentMethodType } from '../utils';

// Modules
import shared from '../../shared';
// import loans from '../../loans'; Rev: movil

// Components
import { PaymentReceipt as PaymentReceiptContainer } from '../containers';
// import { MakePaymentHeader } from '../components';

// const { RefinancingCard } = loans.components;
// const { VirtualCard } = shared.components; Rev: Movil

// Rev: Movil
// type PaymentReceiptComponentProps = NavigationStackScreenProps<{
//   payment: PaymentType;
// }> &
//   ThemedComponentProps;

import '../styles/payments-receipt.scss'
import '../../../assets/styles/cta-footer.scss'

const PaymentReceiptComponent = ({ 
    // navigation, themedStyle 
}
    // : PaymentReceiptComponentProps
    ) => {
//   const payment = navigation.getParam('payment');
    const paymentMethod: PaymentMethodType = {
        email: 'romario',
        methodName: 'string',
        isACH: true,
        methodType: 'string',
        mode: 'string',
        paymentMethodId: 'string',// This is done to avoid multi type id
        accountId: 'string',
        cardNumber: 'string',
        expiryDate: 'string',
        createdDate: 'string',
        selectedForAutoPay: true,
        cardType: 'string',
        active: true,
        default: true,
        bankId: 'string',
        bankName: 'string',
        routingNumber: 'string',
        accountingNumber: 'string',
        accountType: 'string',
        // default: true,
      };
    
      const payment: PaymentType =   {
        transactionId: 'string',
        contractId: 'string',
        mode: 'string',
        isAdditionalPayment: true,
        amount: 12345,
        fee: 1,
        total: 10000,
        date: new Date(),
        email: 'string',
        paymentMethod
    }
  return (
    // <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('PaymentReceiptScreen')}>
    //   <ScrollView contentContainerStyle={themedStyle.body}>
    //     <MakePaymentHeader />
    <React.Fragment>
         {/* <View style={[themedStyle.content]}>  */}
          <PaymentReceiptContainer payment={payment} />

          {/* <Layout style={[themedStyle.item, themedStyle.homeButton]}> 
            <button
            //   appearance="link"
            //   onPress={() => navigation.popToTop()}
            //   textStyle={themedStyle.uppercase}
            //   {...shared.helpers.setTestID('BackToLoginFromPaymentReceipt')}
            >
              Go Home
            </button>
           </Layout>

           <RefinancingCard data={loans.utils.testData.refinanceOption} />
          <VirtualCard dismissible={false} /> 
          </View> 
        </ScrollView>
      </SafeAreaView>  */}
    </React.Fragment>
  );
};

const PaymentReceipt = PaymentReceiptComponent

export default PaymentReceipt;

