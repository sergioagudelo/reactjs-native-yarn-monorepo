import React from 'react';

// Modules
import { PaymentMethodType } from '../utils';

type AutopayConsentTermsModalProps = {
  selectedPaymentMethod?: PaymentMethodType;
  consentText: string;
};

const AutopayConsentTermsModal = ({
  consentText,
}: AutopayConsentTermsModalProps) => {

  return (
    <div id="modal-autopay-consent-terms" uk-modal="true">
      <div className="uk-modal-dialog uk-modal-body">
        <button className="uk-modal-close-default" type="button" uk-close="true"></button>
        <h2 className="uk-modal-title">Autopay consent terms</h2>
        <p>{consentText}</p>
      </div>
    </div>
  );
};

export default AutopayConsentTermsModal;