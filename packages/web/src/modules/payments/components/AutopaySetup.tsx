import React, { useState } from 'react';

// Components
import AutopayConsentTermsModal from './AutopayConsentTermsModal';
import AutopayLabelConsent from './AutopayLabelConsent';
import Loading from 'modules/loading/Loading';

// Modules
import type { ApiResponse } from '@customer-portal/common/src/services/utils';
import type { PaymentMethodType } from '@customer-portal/common/src/modules/payments/utils';
import {
  autopayConsentTerms,
  PaymentMethodTypeKeys,
} from '@customer-portal/common/src/modules/payments/constants';
import shared from '../../shared';

type AutopaySetupProps = {
  userFullName: string;
  defaultPaymentMethod?: PaymentMethodType;
  paymentMethods: PaymentMethodType[];
  isLoading: boolean;
  errorMsg?: string;
  setAsAutopay: (item: PaymentMethodType) => Promise<ApiResponse>;
  onNewPaymentMethod: () => void;
};

const AutopaySetup = ({
  paymentMethods,
  isLoading,
  userFullName,
  setAsAutopay,
  errorMsg,
}: AutopaySetupProps) => {
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState<
    PaymentMethodType | undefined
  >();
  const [isConsentAgreed, setIsConsentAgreed] = useState(false);

  const handleSetAsAutopay = async () => {
    if (selectedPaymentMethod) {
      await setAsAutopay(selectedPaymentMethod);
    }
  };

  return (
    <div>

      <p>
        Choose the payment method you want to setup as autopay
      </p>
      {paymentMethods.map((option, i) => (
        <React.Fragment key={option.paymentMethodId + i}>
          {option.methodName}
          {shared.helpers.format.formatPaymentMethodNumber(option)}
          <input
            type="radio"
            name="paymentMethodOption"
            onClick={(e) => {
              if (isLoading) return;
              console.log(option)
              setSelectedPaymentMethod(option);
            }}
          />
          {i !== paymentMethods.length - 1 && (
            <hr />
          )}
        </React.Fragment>
      ))}

      <AutopayLabelConsent
        userFullName={userFullName}
        paymentMethod={selectedPaymentMethod}
      />

      <AutopayConsentTermsModal
        selectedPaymentMethod={selectedPaymentMethod}
        consentText={
          autopayConsentTerms[
          selectedPaymentMethod
            ? selectedPaymentMethod.mode
            : PaymentMethodTypeKeys.BANK_ACCOUNT
          ]
        }
      />

      {selectedPaymentMethod ? (
        <React.Fragment>
          <input
            type="checkbox"
            defaultChecked={isConsentAgreed}
            onChange={(e) => {
              setIsConsentAgreed(e.target.checked)
            }}
          />
          <p>
            Yes, I have read the terms and conditions specified, and I agree to authorize this
            recurring transaction.
          </p>
        </React.Fragment>
      ) : null}
      {errorMsg ? (
        <p>
          {errorMsg}
        </p>
      ) : null}
      {isLoading ? (
        <Loading />
      ) : (
          <button
            disabled={!paymentMethods || !selectedPaymentMethod || !isConsentAgreed}
            onClick={handleSetAsAutopay}
          >
            Enroll in autopay
          </button>
        )}
    </div >
  );
};

export default AutopaySetup;