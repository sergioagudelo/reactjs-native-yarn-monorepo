import React from 'react';
import { FormikProps, Field, ErrorMessage } from 'formik';

// Components & Modules
import { LoanDetailsBox } from '../../loans/components';
import { ExtraPaymentFormValues } from '../utils';
import { formKeys, forms } from '../constants';
import shared from '../../shared';
import { decimalInputHandler } from '../helpers';

// Styles & Images
import bgExtrapayments from 'images/bg-extrapayments.svg';
import icoError from 'images/icons/ico-error.svg';


type ExtraPaymentProps = {
  onToggle: (checked: boolean) => void;
  isEnabled: boolean;
  extraPaymentMaxAmount: number;
} & FormikProps<ExtraPaymentFormValues>;

const ExtraPaymentComponent = ({
  onToggle,
  isEnabled,
  extraPaymentMaxAmount,
  touched,
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isValid,
  values,
  setFieldValue,
}: ExtraPaymentProps) => {
  const parseAndHandleChange = (value: string, formKey: string) => {
    setFieldValue(formKey, decimalInputHandler(value));
    handleChange(formKey);
  };

  return (
    <div className="uk-width-1-1">
      <div className="uk-card uk-card-default lcp-details lcp-extrapayments uk-grid-collapse" uk-grid="true">
        <div className="uk-card-media-left uk-cover-container uk-width-1-3">
          <img src={bgExtrapayments} alt="bgExtrapayments" uk-cover="true" />
          <div className="uk-position-center">
            <h2 className="uk-text-lead uk-text-bluedark uk-text-center uk-margin-remove">Extrapayments</h2>
          </div>
          <canvas width="200" height="150"></canvas>
        </div>
        <div className="uk-width-2-3 uk-grid-item-match">
          <div className="uk-card-body uk-flex-middle uk-grid-collapse" uk-grid="true">
            <div className="uk-width-1-1 lcp-toggle uk-flex uk-flex-middle uk-flex-left">
              <div className="uk-width-auto uk-text-left uk-text-small uk-text-graydarkx uk-text-uppercase uk-margin-small-right">
                <div className="check-toggle">
                  <input type="checkbox" className="checkbox" uk-toggle="target: .lcp-toggle-form; animation: uk-animation-fade;  duration: 900" />
                  <span className="switch"></span>
                  <span className="track"></span>
                </div>
              </div>
              <div className="uk-text-left uk-text-content uk-text-bluedark">
                I'd like to pay down my loan sooner bfoy making an extra payment.
              </div>
            </div>
            <div className="lcp-toggle-form uk-width-1-1" hidden>
              <div className="lcp-extrapayments-form uk-text-center">
                <label className="uk-form-label uk-text-center">Enter extra payment amount</label>
                <div className="uk-form-controls uk-flex uk-flex-center">
                  <Field
                    className="uk-input uk-form-width-medium uk-margin-small-right"
                    type="number"
                    required
                    onBlur={handleBlur(formKeys.extraPayment.amount)}
                    value={
                      values[formKeys.extraPayment.amount]
                        ? values[formKeys.extraPayment.amount].toString()
                        : values[formKeys.extraPayment.amount] || ''
                    }
                    onChange={(e: any) => parseAndHandleChange(e.target.value, formKeys.extraPayment.amount)}

                    placeholder={forms.extraPayment.placeholders[formKeys.extraPayment.amount](
                      extraPaymentMaxAmount,
                    )}
                  />
                  <button
                    className="uk-button uk-button-primary uk-button-medium"
                    disabled={!isValid}
                    onClick={() => handleSubmit}
                  >
                    Continue
                  </button>
                </div>
                {errors[formKeys.extraPayment.amount] &&
                  <span className="validation-error uk-text-danger">
                    <img className="uk-icon" src={icoError} alt="Error" />
                    {errors[formKeys.extraPayment.amount]}
                  </span>
                }
                {errors.submit && (
                  <div className="error">
                    <span className="validation-error uk-text-danger">
                      <img className="uk-icon" src={icoError} alt="icoError" />
                        Information required
                      </span>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExtraPaymentComponent;
