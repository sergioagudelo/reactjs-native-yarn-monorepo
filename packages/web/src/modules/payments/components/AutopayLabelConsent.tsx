import React from 'react';
import { PaymentMethodType } from '@customer-portal/common/src/modules/payments/utils';

import shared from '../../shared';

const { format } = shared.helpers;

type AutopayLabelConsentProps = {
  userFullName: string;
  paymentMethod?: PaymentMethodType;
  dueDate?: string | undefined;
};

const AutopayLabelConsent = ({
  userFullName,
  paymentMethod,
}: AutopayLabelConsentProps) => {
  if (!userFullName || !paymentMethod) {
    return null;
  }
  const { isACH } = paymentMethod;

  const methodNumber = format.formatPaymentMethodNumber(paymentMethod);

  return isACH ? (
    <div>
      {`By clicking below to make your scheduled payments, you, ${userFullName}, authorize LendingPoint to electronically debit your above bank account ending with ${methodNumber} and routing number ending with 
        ${format.formatBankAccountNumber(
        paymentMethod.routingNumber.toString()
      )} in accordance with the `}
      <button className="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-autopay-consent-terms">
        ACH Consent Terms
      </button>
      for amounts owing on each payment date scheduled in your Loan Agreement or
      as changed thereafter changed thereafter ("Due Date").
    </div>
  ) : (
      <div>
        You authorize LendingPoint to process your debit card for the amounts
        owing on each payment date scheduled in the Loan Agreement or as changed
        thereafter (“Due Date”) in accordance with the
        <button className="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-autopay-consent-terms">
          Debit Consent Terms
        </button>
          You understand the amount of each payment will show up on your bank
          statement for the purposes of payment and amount verification.
      </div >
    );
};

export default AutopayLabelConsent;
