import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { PaymentMethodType } from '../../utils';

import { actions } from '../actions';

// TODO: Type action
const methods = createReducer(null as PaymentMethodType[] | null)
  .handleAction(actions.fetchPaymentMethods.success, (state, action) => (action as any).payload)
  .handleAction(actions.updatePaymentMethods, (state, action) => (action as any).payload);

const methodsReducer = combineReducers({
  methods,
});

export default methodsReducer;
export type PaymentsState = ReturnType<typeof methodsReducer>;
