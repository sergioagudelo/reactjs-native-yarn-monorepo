import { createAsyncAction, createAction } from 'typesafe-actions';
import types from './types';
import { PaymentMethodType } from '../../utils';

export const updatePaymentMethods = createAction(types.PAYMENT_METHODS.UPDATE, action => {
  return (methods: PaymentMethodType[]) => action(methods);
});

export const fetchPaymentMethods = createAsyncAction(
  types.PAYMENT_METHODS.ATTEMPT,
  types.PAYMENT_METHODS.SUCCESS,
  types.PAYMENT_METHODS.FAILURE,
)<undefined, PaymentMethodType[] | null, Error | undefined>();
