import { NAME } from '../../constants';

import shared from '../../../shared';

export default shared.helpers.createActionTypes(NAME, [
  'PAYMENT_METHODS.UPDATE',
  'PAYMENT_METHODS.ATTEMPT',
  'PAYMENT_METHODS.SUCCESS',
  'PAYMENT_METHODS.FAILURE',
]);
