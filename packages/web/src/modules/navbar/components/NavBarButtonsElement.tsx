import React from 'react';
import { NavLink } from 'react-router-dom';

// Assets & Images
import imgNotification from 'images/nav/notification.svg';
import imgHome from 'images/nav/home.svg';
import imgOffers from 'images/nav/offers.svg';
import imgAccount from 'images/nav/account.svg';
import imgEducation from 'images/nav/education.svg';
import imgHelp from 'images/nav/help.svg';
import imgLogOut from 'images/nav/logout.svg';

const NavBarButtonsElement = () => {
  return (
    <div className="uk-navbar-right">
      <ul className="lcp-iconnav uk-iconnav uk-visible@m">
        {/* <li className="uk-active">
              <a className="uk-icon-nav uk-icon" href="/"><img src={imgHome} alt="Home" /></a>
              </li> */}
        <li>
          <NavLink to="/loans" activeClassName="uk-active">
            <img src={imgHome} alt="imgHome" />
          </NavLink>
        </li>
        <li>
          <NavLink to="/" activeClassName="uk-active" exact>
            <img src={imgNotification} alt="imgNotification" />
          </NavLink>
        </li>
        <li>
          <NavLink to="/" activeClassName="uk-active" exact>
            <img src={imgOffers} alt="imgOffers" />
          </NavLink>
        </li>
        <li>
          <NavLink to="/" activeClassName="uk-active" exact>
            <img src={imgAccount} alt="imgAccount" />
          </NavLink>
        </li>
        <li>
          <NavLink to="/auth/forgot-password" activeClassName="uk-active" exact>
            <img src={imgEducation} alt="imgEducation" />
          </NavLink>
        </li>
        <li>
          <NavLink to="/profile" activeClassName="uk-active" exact>
            <img src={imgHelp} alt="imgHelp" />
          </NavLink>
        </li>
        <li>
          <NavLink to="/auth" activeClassName="uk-active" exact>
            <img src={imgLogOut} alt="LogOut" />
          </NavLink>
        </li>
      </ul>
      <button className="uk-navbar-toggle uk-hidden@m" uk-navbar-toggle-icon="true" uk-toggle="target: #lcp-offcanvas"></button>
    </div>
  );
};

export default NavBarButtonsElement;