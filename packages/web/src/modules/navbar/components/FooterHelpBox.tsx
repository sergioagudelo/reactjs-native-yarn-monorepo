import React from 'react';

const FooterHelpBox = () => {
  return (
    <div className="lcp-helpbox uk-text-center uk-width-1-1 uk-flex uk-flex-center uk-flex-middle">
      <small className="lcp-phone uk-text-bluedark uk-text-uppercase uk-margin-remove">
        Need help? <a className="uk-text-orange" href="tel:8889690959">(888) 969-0959</a>
      </small>
      <small className="lcp-hours uk-text-graydarkx uk-text-uppercase">
        Monday - Friday 8am-9pm • Saturday 10am-4pm
      </small>
    </div>
  );
};

export default FooterHelpBox;