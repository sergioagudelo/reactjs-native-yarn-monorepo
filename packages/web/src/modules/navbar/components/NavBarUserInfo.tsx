import React from 'react';
import { useLocation } from 'react-router-dom';

// Constants & Helpers
import { titles } from '../constants';

// Store
import * as store from '../../profile/store';
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

// Types
type TitlesTypes = keyof typeof titles & 'emptyPath';

const mapStateToProps = (state: RootState) => ({
  userInfo: store.selectors.getUserInfo(state),
});

type NiceToSeeYouProps = ReturnType<typeof mapStateToProps>;

const NavBarUserInfo = ({ userInfo }: NiceToSeeYouProps) => {

  let pathname = useLocation().pathname.replaceAll('/', '') as TitlesTypes || 'emptyPath';
  let navBarHeader = titles[pathname] || { subtitle: 'PAGE NOT FOUND' };

  return (
    <div className="lcp-heading uk-text-center uk-width-expand">
      <div className="uk-heading-small">
        {userInfo && !navBarHeader['title'] ? `Hi, ${userInfo.firstName}. Nice to see you again` : navBarHeader['title']}
      </div>
      <div className="uk-text-meta">{navBarHeader ? navBarHeader['subtitle'] : ''}</div>
    </div>
  );
};

export default connect(mapStateToProps)(NavBarUserInfo);