
export const titles = {
  emptyPath: { title: null, subtitle: null },
  auth: { title: null, subtitle: 'ENTER YOUR ACCOUNT' },
  profile: { title: 'Your Account', subtitle: 'INFORMATION & SETTINGS' },
  loans: { title: null, subtitle: 'LOAN DASHBOARD' },
  loansloanDetails: { title: 'Loan Details', subtitle: 'DETAILS & PAYMENTS' },
  loansloanDetailspaymentsHistory: { title: 'Payment History', subtitle: 'LATEST PAYMENTS DETAILS' },
  loansloanDetailsautopaySetup: { title: 'Loan AutopaySetup', subtitle: 'LOAN AUTPAYENT SETUP' },
} as const;
