import React from 'react';

// Components
import FooterHelpBox from '../components/FooterHelpBox';

const Footer = () => {
  return (
    <React.Fragment>
      <FooterHelpBox />
      <div className="lcp-footer uk-text-center uk-flex uk-flex-middle uk-flex-around uk-light">
        <div className="uk-width-1-3@s uk-card uk-text-small">
          ©2020, LendingPoint LLC. All rights reserved
        </div>
        <div className="uk-width-1-3@s uk-card uk-text-small">
          <a href="/" target="_blank">Terms of Use</a>
          <span>   |   </span>
          <a href="/" target="_blank">Privacy Notice</a>
        </div>
        <div className="uk-width-1-3@s uk-card uk-text-small">
          1201 Roberts Blvd Suite 200 Kennesaw, GA 30144
        </div>
      </div>
    </React.Fragment>
  );
};

export default Footer;