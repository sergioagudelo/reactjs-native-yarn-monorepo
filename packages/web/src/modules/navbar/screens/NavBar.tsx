import React from 'react';
import { NavLink } from 'react-router-dom';

// Components
import NavBarUserInfo from '../components/NavBarUserInfo';
import NavBarButtonsElement from '../components/NavBarButtonsElement';

// Constatns & Helpers

// Assets & Images
import './../styles/NavBar.scss';
import imgLendingpoint from 'images/lendingpoint.svg';
import imgNotification from 'images/nav/notification.svg';
import imgHome from 'images/nav/home.svg';
import imgOffers from 'images/nav/offers.svg';
import imgAccount from 'images/nav/account.svg';
import imgEducation from 'images/nav/education.svg';
import imgHelp from 'images/nav/help.svg';
import imgLogOut from 'images/nav/logout.svg';

// Store
import * as store from '../../profile/store';
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

const mapStateToProps = (state: RootState) => ({
  userInfo: store.selectors.getUserInfo(state),
});

type NavBarProps = ReturnType<typeof mapStateToProps>

const NavBar = ({ userInfo }: NavBarProps) => {

  return (
    <React.Fragment>
      {userInfo ?
        <>

          {/* WEB NAVBAR */}

          <div className="uk-container lcp-navigation">
            <nav className="uk-navbar-container uk-navbar-transparent uk-navbar">
              <div className="uk-navbar-left uk-width-expand">
                <ul className="uk-navbar-nav uk-flex uk-flex-middle uk-flex-between">
                  <li>
                    <NavLink to="/" className="uk-navbar-item uk-logo">
                      <img src={imgLendingpoint} alt="" />
                    </NavLink>
                  </li>
                  <li>
                    <a className="uk-navbar-item uk-logo" href="/" hidden>
                      <img src="images/lendingpoint.svg" alt="" />
                    </a>
                  </li>
                  <li>
                    <span className="uk-navbar-item uk-logo" hidden>
                      <img src="images/ebay.png" alt="" />
                    </span>
                  </li>
                </ul>
              </div>
              <div className="uk-navbar-center">
                <NavBarUserInfo />
              </div>

              <div className="uk-navbar-right">
                {/* <NavBarButtonsElement /> */}
                <button className="uk-navbar-toggle" uk-navbar-toggle-icon="true" uk-toggle="target: #lcp-offcanvas"></button>
              </div>

            </nav>
          </div>

          {/* MOBILE NAVBAR */}

          <div id="lcp-offcanvas" uk-offcanvas="mode: reveal; overlay: true; flip: true">
            <div className="uk-offcanvas-bar">
              <button className="uk-offcanvas-close" uk-close="true" type="button"></button>
              <ul className="uk-nav uk-nav-default uk-margin-top lcp-offcanvas-nav">
                <li className="uk-nav-header">Menu</li>
                <li className="uk-nav-divider"></li>
                <li>
                  <NavLink to="/loans" activeClassName="uk-active" exact>
                    <img src={imgHome} alt="" />Home
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/" activeClassName="uk-active" exact>
                    <img src={imgNotification} alt="" />Messages
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/" activeClassName="uk-active" exact>
                    <img src={imgOffers} alt="" />Offers
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/profile" activeClassName="uk-active" exact>
                    <img src={imgAccount} alt="" />Account
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/" activeClassName="uk-active" exact>
                    <img src={imgEducation} alt="" />Life
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/" activeClassName="uk-active" exact>
                    <img src={imgHelp} alt="" />Center
                  </NavLink>
                </li>
                <li className="uk-nav-divider"></li>
                <li>
                  <NavLink to="/auth" className="uk-icon-nav uk-icon" activeClassName="uk-active" exact>
                    <img src={imgLogOut} alt="LogOut" />Logout
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </> : ''}
    </React.Fragment>
  );
};

export default connect(mapStateToProps)(NavBar);
