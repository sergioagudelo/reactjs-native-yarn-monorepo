import React, { Suspense } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

// Components
import RootRoutes from './navigation/RootRoutes';
import Loading from './modules/loading/Loading';
import NavBar from 'modules/navbar/screens/NavBar';
import Footer from 'modules/navbar/screens/Footer';

// Store
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './store';

// Shared code
import { log, log2 } from '@customer-portal/common';
import Test from '@customer-portal/common/src/components/Test'

const App = () => {
  log("prueba");
  log2("otra prueb")
  return (
    <Suspense fallback={<Loading loadingText="Loading" />}>
      <Provider store={store}>
        <PersistGate
          loading={
            <Loading />
          }
          persistor={persistor}
        >
          <Router>
            <NavBar />
            <div className="uk-section uk-section-overlap uk-preserve-color uk-padding-remove-vertical">
              <div className="lcp-main">
                <RootRoutes />
              </div>
            </div>
          </Router>
          {/* <Test /> */}
          <Footer />
        </PersistGate>
      </Provider>
    </Suspense>
  );
}

export default App;
